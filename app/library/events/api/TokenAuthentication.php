<?php

/**
 * Event that Authenticates the client message with HMac
 *
 * @package Events
 * @subpackage Api
 * @author Jete O'Keeffe
 * @version 1.0
 */

namespace Events\Api;

use Interfaces\IEvent as IEvent;

class TokenAuthentication extends \Phalcon\Events\Manager implements IEvent {

    /**
     * Hmac Message
     * @var object
     */
    protected $_token;

    /**
     * Private key for HMAC
     * @var string
     */
    protected $_privateKey;

    /**
     * Constructor
     *
     * @param object
     * @param string
     */
    public function __construct($message, $privateKey) {
        $this->_token = $message;
        $this->_privateKey = $privateKey;

        // Add Event to validate message
        $this->handleEvent();
    }

    /**
     * Setup an Event
     *
     * Phalcon event to make sure client sends a valid message
     * @return FALSE|void
     */
    public function handleEvent() {

        $this->attach('micro', function ($event, $app) {

            if ($event->getType() == 'beforeExecuteRoute') {

                // Authenticating the Token If expired or something is wrong in decoding
                $jwt = new \Security\Jwt\JWT();

                try {
                    // Cache data for 2 days
                    $frontCache = new \Phalcon\Cache\Frontend\Data(array(
                        "lifetime" => $app->config->redis->dataexpiration
                    ));

                    //Create the Cache setting redis connection options
                    $cache = new \Phalcon\Cache\Backend\Redis($frontCache, array(
                        'host' => $app->config->redis->host,
                        'port' => $app->config->redis->port,
                        'persistent' => $app->config->redis->persistent
                    ));
                }
                catch (Exception $e) {
                    die($e->getMessage());
                }

                $parsetoken = explode(" ",$this->_token);

                $token = $jwt->decode($parsetoken[1], $this->_privateKey, array('HS256'));

                $allowed = true;
                $public = false;
                $clientKey = false;
                if(is_array(($token))){
                    if(isset($token['error'])) {
                        $allowed = false;
                        $cache->delete($app->config->redis->sessionkey . $parsetoken[1]);
                    }

                }

                if(empty($cache->get($app->config->redis->sessionkey.$token->id))){
                    $allowed = false;
                }

                $method = strtolower($app->router->getMatchedRoute()->getHttpMethods());

                $unAuthenticated = $app->getUnauthenticated();

                if (isset($unAuthenticated[$method])) {
                    $unAuthenticated = array_flip($unAuthenticated[$method]);

                    if (isset($unAuthenticated[$app->router->getMatchedRoute()->getPattern()])) {
                        $allowed = true;
                        $public = true;
                        $clientKey = true;
                    }
                }



                // Check if the user is a valid client for the API
                $clients = $app->config->clients;

                $levels = $app->getLevels();
                $alliswell = false;
                if (isset($levels['all'])) {

                    $all = array_flip($levels['all']);

                    if (isset($all[$app->router->getMatchedRoute()->getPattern()]) && !$public) {
                        $alliswell = true;
                    }
                }


                foreach( $clients as $key => $val){
                    if($val === $token->level){
                        $clientKey = true;
                        $level = array_flip($levels[$key]);
                        if (!isset($level[$app->router->getMatchedRoute()->getPattern()]) && !$public && !$alliswell) {
                            $allowed = false;
                        }
                        break;
                    }
                }

                if (!$allowed || !$clientKey) {
                    $app->response->setStatusCode(401, "Unauthorized");
                    $json = json_encode(array(
                        'Unauthorize' => 'SHU! GO AWAY!'
                    ));
                    $app->response->setContent($json);
                    $app->response->send();

                    return false;
                }
            }
        });
    }
}
