<?php

namespace Controllers;

use \Models\Users as Users;
use \Controllers\ControllerBase as CB;

class UserController extends \Phalcon\Mvc\Controller {

    /**
     * @SWG\Post(
     *     path="/agent/login",
     *     summary="Agent Login",
     *     tags={"Agent"},
     *     description="Login Agent and return token",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Login Credentials",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/AgentLogin")
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation"
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="Invalid tag value"
     *     )
     * )
     *
     * @SWG\Definition(
     *      definition="AgentLogin",
     *      @SWG\Property(property="email", type="string"),
     *      @SWG\Property(property="password", type="string"),
     * )
     */

    public function loginAction() {
        $app = new CB();
        $request = new \Phalcon\Http\Request();
        $user = Users::findFirst('(email="' .  $request->getPost('username').'" OR username="' .$request->getPost('username').'") AND  password="'. sha1($request->getPost('password')).'" AND status=1');
        if($user) {
            $jwt = new \Security\Jwt\JWT();

            $timestamp = time();

            $access_token = array(
                "id" => $user->id,
                "username" => $user->username,
                "lastname" => $user->last_name,
                "firstname" => $user->first_name,
                "level" => $app->getConfig()['clients']['admin'],
                "exp" => $timestamp + (60 * 60)); // time in the future

            $token = $jwt->encode($access_token, $app->getConfig()['hashkey']);

            $guid = new \Utilities\Guid\Guid();
            $refresh_token = array(
                "id" => $guid->GUID(),
                "exp" => strtotime('+1 day', $timestamp),
                "created" => $timestamp
            );

            $atoken = $jwt->encode($access_token, $app->getConfig()['hashkey']);
            $rtoken = $jwt->encode($refresh_token, $app->getConfig()['hashkey']);

            $app->storeRedis($user->id,$atoken);

            echo json_encode(array('access' => $atoken, 'refresh' => $rtoken));
        }else{
            echo json_encode(array('error' => 'Username or Password is invalid.'));
        }
    }

    public function logoutAction(){
        $request = new \Phalcon\Http\Request();
        $jwt = new \Security\Jwt\JWT();
        $parsetoken = explode(" ",$request->getHeader('Authorization'));
        $app = new CB();
        $token = $jwt->decode($parsetoken[1], $app->getConfig()['hashkey'], array('HS256'));

        if($app->removeRedis($token->id)){
            die(json_encode(array("success" => 'Succcess')));
        }else{
            die(json_encode(array("error" => 'Succcess')));
        }

    }
}
