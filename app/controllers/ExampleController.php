<?php

namespace Controllers;

use \Controllers\ControllerBase as CB;
use \Controllers\GlobalController as GC;

class ExampleController {


	public function pingAction() {

            $app = new GC;

        $sql = 'SELECT
            IF(maps.hide_agent=1,agents.first_name,"Anonymous") as first_name ,
            IF(maps.hide_agent=1,agents.last_name,"") as last_name,
            agents.email,
            agents.id as agentid,
            agents.username,
            agents.profile_pic_name,
            maps.title,
            maps.description,
            maps.created_at,
            maps.updated_at,
            maps.id as mapid,
            maps.featured,
            maps.greatest,
            maps.views,
            maps.cover,
            maps.coverType,
            maps.status,
            maps.mapslugs,
            maps.hide_agent,
            count(*) as actions,
            COALESCE(mapcom.countComments,0) AS countComments,
            COALESCE(commentrep.countRepComments,0) AS countRepComments

              FROM

              maps INNER JOIN agents ON maps.agent = agents.id INNER JOIN markers ON maps.id = markers.map_id

                LEFT JOIN (SELECT COUNT(*) AS countComments, map_id
                        FROM mapcomments WHERE type != "pin"
                        GROUP BY map_id) AS mapcom ON mapcom.map_id = maps.id

                LEFT JOIN (SELECT COUNT(*) AS countRepComments, map_id
                        FROM commentreply
                        GROUP BY map_id) AS commentrep ON commentrep.map_id = maps.id

              ';

        $sqlFeatured = $sql . " WHERE maps.featured = '1' AND maps.status=1 GROUP BY maps.id LIMIT 4 ";
        $searchResultFeatured = $app->dbSelect($sqlFeatured);

        $sqlGreatest = $sql . " WHERE maps.greatest = '1' AND maps.status=1 GROUP BY maps.id LIMIT 3";
        $searchResultGreatest= $app->dbSelect($sqlGreatest);

        $sqlLatest = $sql . " WHERE maps.status=1 GROUP BY maps.id ORDER BY created_at DESC LIMIT 3";
        $searchResultLatest = $app->dbSelect($sqlLatest);

        $sqlPopular = $sql . " WHERE maps.status=1 GROUP BY maps.id ORDER BY maps.views DESC LIMIT 3";
        $searchResultPopular = $app->dbSelect($sqlPopular);


        //Getting Categories
        $sqlMissionCategories = 'SELECT mapcategories.*, COUNT(mapcats.id) AS post_count FROM mapcategories LEFT JOIN mapcats ON mapcategories.id = mapcats.catid GROUP BY mapcategories.id ORDER BY post_count DESC LIMIT 4';
        $searchResultCategories = $app->dbSelect($sqlMissionCategories);

        $sql = "SELECT * FROM news WHERE status=1 ORDER BY created_at DESC LIMIT 3";
        $news = $app->dbSelect($sql);

        die(json_encode(array(
            'featured'=> $searchResultFeatured,
            'greatest'=> $searchResultGreatest,
            'latest'=> $searchResultLatest,
            'popular'=> $searchResultPopular,
            'categories' => $searchResultCategories,
            'news' => $news
        )));

//        try {
//            // Cache data for 2 days
//            $frontCache = new \Phalcon\Cache\Frontend\Data(array(
//                "lifetime" => 172800
//            ));
//
//            //Create the Cache setting redis connection options
//            $cache = new \Phalcon\Cache\Backend\Redis($frontCache, array(
//                'host' => 'localhost',
//                'port' => 6379,
//                'persistent' => false
//            ));
//
//            $request = new \Phalcon\Http\Request();
//            $jwt = new \Security\Jwt\JWT();
//            $parsetoken = explode(" ",$request->getHeader('Authorization'));
//            $app = new CB();
//            $token = $jwt->decode($parsetoken[1], $app->config->hashkey, array('HS256'));
//            $data = $cache->get('sess'.$token->id);
//            var_dump($data);
//        }
//        catch (Exception $e) {
//            die($e->getMessage());
//        }

	}


    public function testAction($id) {
        echo "test (id: $id)";
    }

    public function skipAction($name) {
        echo "auth skipped ($name)";
    }
}
