<?php

namespace Controllers;

use \Controllers\ControllerBase as CB;
use \Models\Contacts as Contacts;
use \Models\Contactsreply as Contactsreply;


class ContactsController extends \Phalcon\Mvc\Controller {


	public function savefeedbackAction() {
       $app = new CB();
       $request = new \Phalcon\Http\Request();
       if($request->isPost()){
            $c = new Contacts();
            $guid = new \Utilities\Guid\Guid();
            $c->id = $guid->GUID();
            $c->firstname = $request->getPost("firstname");
            $c->lastname = $request->getPost("lastname");
            $c->email = $request->getPost("email");
            $c->subject = $request->getPost("subject");
            $c->message = $request->getPost("message");
            $c->created_at = date("Y-m-d H:i:s");
            $c->status = 1;

            if($c->save()){
                $app->sendMail($request->getPost("email"), $request->getPost("subject"), $request->getPost("message"));
                echo json_encode(array('success' => 'Your message has been successfully sent.'));
            }else {
                echo json_encode(array('error' => 'An error occurred please try again later.'));
            }
       }
	}

	public function getlistAction($num, $off, $keyword, $date) {
		$app = new CB();

        // offsetting
        $offsetfinal = ($off * 10) - 10;
        $sql = 'SELECT * FROM contacts';
        $sqlCount = 'SELECT COUNT(*) FROM contacts';

        if ($keyword != 'null' && $keyword != 'undefined' && ($date == 'null' || $date == 'undefined')) {
            $sqlWhere = " WHERE firstname LIKE '%" . $keyword . "%' OR lastname LIKE '%" . $keyword . "%' OR subject LIKE '%" . $keyword . "' OR email LIKE '%".$keyword."%' OR message LIKE '%".$keyword."%'";
            $sql .= $sqlWhere;
            $sqlCount .= $sqlWhere;
        }else if(($keyword == 'null' || $keyword == 'undefined')  && $date != 'null' && $date != 'undefined'){
            $sqlWhere = " WHERE DATE_FORMAT(created_at,'%Y-%m-%d' ) = '".$date."'";
            $sql .= $sqlWhere;
            $sqlCount .= $sqlWhere;
        } else if($keyword != 'null' && $keyword != 'undefined' && $date != 'null' && $date != 'undefined'){
			$sqlWhere = " WHERE (firstname LIKE '%" . $keyword . "%' OR lastname LIKE '%" . $keyword . "%' OR subject LIKE '%" . $keyword . "' OR email LIKE '%".$keyword."%' OR message LIKE '%".$keyword."%') AND DATE_FORMAT(created_at,'%Y-%m-%d' ) = '".$date."'";
			$sql .= $sqlWhere;
			$sqlCount .= $sqlWhere;
		}

        if($offsetfinal < 0){
            $offsetfinal = 0;
        }

        $sql .= " ORDER BY created_at DESC ";
        $sql .= " LIMIT " . $offsetfinal . ",10";
        // getting the query
        $searchresult = $app->dbSelect($sql);

        $totalreportdirty = $app->dbSelect($sqlCount);

        echo json_encode(array('data' => $searchresult, 'index' =>$off, 'total_items' => $totalreportdirty[0]["COUNT(*)"]));
	}

	public function deleteAction($id) {
		$contact = Contacts::findFirst("id='$id'");
		if($contact){
			if($contact->delete()){
				echo "Message has been deleted.";
			}else {
				echo "An error occurred please try again later.";
			}
		}else {
			echo "An error occurred please try again later.";
		}
	}

	public function getAction($id){
		$app = new CB();
		$contact = Contacts::findFirst("id='$id'");
		if($contact){
			if($contact->status == 1){
				$contact->status = 2;
				$contact->save();
			}else if($contact->status == 3){
				$reply = $app->dbSelect("SELECT * FROM contactsreply WHERE contactsid='$id' ORDER BY created_at ASC");
				die(json_encode(array('contact' => $contact, 'reply' => $reply)));
			}
			echo json_encode($contact->toArray());
		}
	}

	public function replyAction($id) {
		$request = new \Phalcon\Http\Request();
		$guid = new \Utilities\Guid\Guid();
		$app = new CB();

		if($request->isPost()){
			$reply = new Contactsreply();
            $reply->id = $guid->GUID();
			$reply->contactsid = $id;
			$reply->message = $request->getPost("reply");
			$reply->created_at = date("Y-m-d H:i:s");

			if($reply->save()){
				$c = Contacts::findFirst("id='$id'");
				$c->status = 3;
				$c->save();

			    $body = $c->subject . "<br><br>";
			    $body .= "Hi ".$c->name.",<br><br>".$request->getPost("reply")."<br><br>====================================================<br><br>";
			    $body .= "Your Message:";
			    $body .= "<br><br>".$c->message;
			    $app->sendMail($c->email, 'Re -'.$c->subject, $body);

				$data = array('success' => 'success' );
			}else {
				$data = array('error' => 'An error occurred please try again later.' );
			}
			echo json_encode($data);
		}
	}
}
