<?php

namespace Controllers;

use \Models\Agents as Agents;
use \Models\Users as Users;
use \Models\Maps as Maps;
use \Models\Markers as Markers;
use \Models\Markerpics as Markerpics;
use \Models\Maplikes as Maplikes;
use \Models\Markerlikes as Markerlikes;
use \Models\Mapcomments as Mapcomments;
use \Models\Commentreply as Commentreply;
use \Controllers\ControllerBase as CB;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class CommentController extends \Phalcon\Mvc\Controller {

	public function savecommentAction($id, $agent, $type, $pinid) {
        $request = new \Phalcon\Http\Request();
        if($request->isPost()){
            $comment = new Mapcomments();
            $guid = new \Utilities\Guid\Guid();
            $comment->id = $guid->GUID();
            $comment->comment = preg_replace('{(<br(\s*/)?>|&nbsp;|<p>&nbsp;<p>)+$}i', '', $request->getPost('comment'));
            $comment->map_id = $id;
            $comment->agent = $agent;
            $comment->created_at = date("Y-m-d H:i:s");
            $comment->updated_at = date("Y-m-d H:i:s");
            $comment->marker_id = $pinid;
            $comment->type = $type;
            $comment->status = 1;
            $comment->superagent = $agent != "superagent" ? 0 : 1;

            if($comment->save()){
            	$data = array('success' => 'Comment has been successfully added.');
            }else {
            	$data = array('error' => 'An error occurred please try again later.');
            }
            echo json_encode($data);
        }
    }

    // public function getpinrepliesAction($id) {
    // 	$app = new CB();
    //     $comments = $app->dbSelect("SELECT commentreply.*, agents.first_name, agents.last_name, agents.profile_pic_name FROM commentreply INNER JOIN agents ON commentreply.agent=agents.id WHERE marker_id='$id' ORDER BY commentreply.updated_at DESC");
    //     foreach ($comments as $key => $value) {
    //         $comments[$key]['showedit'] = false;
    //     }
    // 	echo json_encode($comments);
    // }

    public function savepinreplyAction($markerid, $agentid, $mapid) {
    	$request = new \Phalcon\Http\Request();
    	if($request->isPost()){
    		$comment = Mapcomments::findFirst("marker_id='$markerid'");
    		if($comment){
    			$comrep = new Commentreply();
	            $guid = new \Utilities\Guid\Guid();
	            $comrep->id = $guid->GUID();
    			$comrep->mapcomment_id = $comment->id;
    			$comrep->type = 'pin';
    			$comrep->marker_id = $markerid;
    			$comrep->agent = $agentid;
    			$comrep->created_at = date("Y-m-d H:i:s");
    			$comrep->updated_at = date("Y-m-d H:i:s");
    			$comrep->reply = preg_replace('{(<br(\s*/)?>|&nbsp;|<p>&nbsp;<p>)+$}i', '',$request->getPost('reply'));
    			$comrep->map_id = $mapid;
                $comrep->superagent = $agentid != "superagent" ? 0 : 1;
    			if($comrep->save()){
    				$comment->updated_at = date("Y-m-d H:i:s");
    				if($comment->save()){
	    				$app = new CB();
	       				$reply = $app->dbSelect("SELECT commentreply.*, agents.first_name, agents.last_name, agents.username, agents.profile_pic_name FROM commentreply LEFT JOIN agents ON commentreply.agent=agents.id WHERE marker_id='$markerid' ORDER BY commentreply.updated_at DESC");
                        foreach($reply as $key=>$value){
                            $reply[$key]['showedit'] = false;
                        }
                        $data = array('success' => 'Comment has been successfully added.', 'reply' => $reply);
    				}else {
	            		$data = array('error' => 'An error occurred please try again later.');
    				}
    			}else {
	            	$data = array('error' => 'An error occurred please try again later.');
    			}
	            echo json_encode($data);
    		}
    	}
    }

    public function savecommentreplyAction($commentid, $agentid, $mapid, $type, $marker_id, $offset, $count, $commentload) {
    	$request = new \Phalcon\Http\Request();
    	if($request->isPost()){
    		$reply = new Commentreply();
            $guid = new \Utilities\Guid\Guid();
            $reply->id = $guid->GUID();
            $reply->reply = preg_replace('{(<br(\s*/)?>|&nbsp;|<p>&nbsp;<p>)+$}i', '',$request->getPost('reply'));
            $reply->agent = $agentid;
            $reply->mapcomment_id = $commentid;
            $reply->type = $type;
            $reply->marker_id = $marker_id;
            $reply->map_id = $mapid;
            $reply->created_at = date("Y-m-d H:i:s");
            $reply->updated_at = date("Y-m-d H:i:s");
            $reply->superagent = $agentid != "superagent" ? 0 : 1;
            if($reply->save()){
            	$mapcom = Mapcomments::findFirst("id='" . $commentid . "'");
            	$mapcom->updated_at = $reply->created_at;
            	if($mapcom->save()){
            		$app = new CB();
			        $comments = $app->dbSelect("SELECT mapcomments.*,
                    IF(mapcomments.agent != 'superagent', agents.first_name, 'Superagent') as first_name, 
                    IF(mapcomments.agent != 'superagent', agents.last_name, '') as last_name,
                    agents.username,
                    agents.profile_pic_name, markers.hide_agent FROM mapcomments LEFT JOIN agents ON mapcomments.agent=agents.id LEFT JOIN markers ON mapcomments.marker_id = markers.id WHERE mapcomments.map_id='$mapid' ORDER BY mapcomments.updated_at  DESC LIMIT " . $commentload);
                    foreach($comments as $key=>$value){
                        $comments[$key]['showedit'] = false;
                    }
                    $sql = "SELECT commentreply.*,
                        IF(commentreply.agent != 'superagent', agents.first_name, 'Superagent') as first_name, 
                        IF(commentreply.agent != 'superagent', agents.last_name, '') as last_name,
                        agents.username,
                        agents.profile_pic_name FROM commentreply LEFT JOIN agents ON commentreply.agent=agents.id WHERE mapcomment_id='" . $commentid. "' ORDER BY commentreply.created_at ASC LIMIT " . $offset . " OFFSET " . ($offset >= $count ? 0 :($count - ($offset + 1)));
        			$commentreply = $app->dbSelect($sql);
                    foreach($commentreply as $key=>$value){
                        $commentreply[$key]['showedit'] = false;
                    }
	            	$data = array('success' => 'Response has been successfully added.', 'comments' => $comments, 'reply' => $commentreply);
            	}else {
            		$data = array('error' => 'An error occurred please try again later. a');
            	}
            }else {
            	$data = array('error' => 'An error occurred please try again later. b');
            }
            echo json_encode($data);
    	}
    }

    public function getmapcommentsAction($mapid, $limit) {
    	$app = new CB();
    	$repcount = [];
    	$commentreply = [];

        $comments = $app->dbSelect("SELECT mapcomments.*, agents.first_name, agents.last_name, agents.username, agents.profile_pic_name, markers.hide_agent FROM mapcomments LEFT JOIN agents ON mapcomments.agent=agents.id LEFT JOIN markers ON mapcomments.marker_id=markers.id WHERE mapcomments.map_id='$mapid' AND mapcomments.status=1 ORDER BY mapcomments.updated_at DESC LIMIT $limit");
        
        //comments count
        $sql = "SELECT COUNT(*) FROM mapcomments LEFT JOIN agents ON mapcomments.agent=agents.id WHERE map_id='$mapid' ORDER BY mapcomments.updated_at DESC";
        $comcount = $app->dbSelect($sql)[0]["COUNT(*)"];

        foreach ($comments as $key => $value) {
            $sql = "SELECT COUNT(*) FROM commentreply LEFT JOIN agents ON commentreply.agent=agents.id WHERE mapcomment_id='" . $value['id']. "' ORDER BY commentreply.created_at DESC";
            array_push($repcount, array( "count"=>$app->dbSelect($sql)[0]["COUNT(*)"], "id"=>$value['id'], "loaded" => 2));
            $comments[$key]['showedit'] = false;

            $sql = "SELECT commentreply.*, 
                IF(commentreply.agent!='superagent', agents.first_name, 'Superagent') AS first_name,
                IF(commentreply.agent!='superagent', agents.last_name, '') AS last_name,
                agents.username,
                agents.profile_pic_name FROM commentreply LEFT JOIN agents ON commentreply.agent=agents.id WHERE mapcomment_id='" . $value['id']. "' ORDER BY commentreply.created_at ASC LIMIT 2";
            $res = $app->dbSelect($sql);
            foreach($res as $key => $value) {
                $res[$key]['showedit'] = false;
                array_push($commentreply, $res[$key]);
            }
        }
        echo json_encode(array('comments' => $comments, 'commentcount' =>$comcount, 'reply' => $commentreply, 'replycount' => $repcount), JSON_NUMERIC_CHECK);
    }

	public function loadreplyAction($id, $offset, $count){
    	$app = new CB();
		$sql = "SELECT commentreply.*, 
        IF(commentreply.agent != 'superagent', agents.first_name, 'Superagent') as first_name, 
        IF(commentreply.agent != 'superagent', agents.last_name, '') as last_name,
        agents.username,
        agents.profile_pic_name FROM commentreply LEFT JOIN agents ON commentreply.agent=agents.id WHERE mapcomment_id='" . $id. "' ORDER BY commentreply.created_at ASC LIMIT " . $offset . " OFFSET " . ($offset >= $count ? 0 :($count - ($offset + 1)));
		$res = $app->dbSelect($sql);
        foreach($res as $key => $value) {
            $res[$key]['showedit'] = false;
            array_push($commentreply, $res[$key]);
        }
		if($res){
			echo json_encode($res);
		}else {
			echo json_encode(array("error" => "Please try again later."));
		}
	}

    public function deletecommentAction($id, $type) {
        if($type=='comment'){
            $mp = Mapcomments::findFirst("id='$id'");
            if($mp->delete()){
                $mp = Commentreply::find("id='$id'");
                $mp->delete();
                $data = array("success"=>"Comment has been successfully deleted.");
            }else {
                $data = array("error"=>"An error occurred, please try again later");
            }
        }else {
            $mp = Commentreply::findFirst("id='$id'");
            if($mp->delete()){
                $data = array("success"=>"Response has been successfully deleted.");
            }else {
                $data = array("error"=>"An error occurred, please try again later");
            }
        }
        echo json_encode($data);
    }

    public function editcommentAction($id, $type) {
        $request = new \Phalcon\Http\Request();
        if($request->isPost()){
            if($type=='comment'){
                $mp = Mapcomments::findFirst("id='$id'");
                $mp->comment = preg_replace('{(<br(\s*/)?>|&nbsp;|<p>&nbsp;<p>)+$}i', '',$request->getPost('msg'));
                $mp->updated_at = date('Y-m-d H:i:s');
                if($mp->save()){
                    $data = array("success"=>"Comment has been successfully edited.");
                }else {
                    $data = array("error"=>"An error occurred, please try again later");
                }
            }else {
                $mp = Commentreply::findFirst("id='$id'");
                $mp->reply = preg_replace('{(<br(\s*/)?>|&nbsp;|<p>&nbsp;<p>)+$}i', '',$request->getPost('msg'));
                $mp->updated_at = date('Y-m-d H:i:s');
                if($mp->save()){
                    $mp = Mapcomments::findFirst("id='$mp->mapcomment_id'");
                    $mp->updated_at = date('Y-m-d H:i:s');
                    if($mp->save()){
                        $data = array("success"=>"Response has been successfully edited.");
                    }else {
                        $data = array("error"=>"An error occurred, please try again later");
                    }
                }else {
                    $data = array("error"=>"An error occurred, please try again later");
                }
            }
            echo json_encode($data);
        }
    }

    public function deleteresponseAction($id) {
        $mp = Commentreply::findFirst("id='$id'");
        if($mp->delete()){
            $data = array("success"=>"Comment has been successfully deleted.");
        }else {
            $data = array("error"=>"An error occurred, please try again later");
        }
        echo json_encode($data);
    }

    public function editresponseAction($id) {
        $request = new \Phalcon\Http\Request();

        if($request->isPost()){
            $mp = Commentreply::findFirst("id='$id'");
            if($mp){
                $mp->reply = preg_replace('{(<br(\s*/)?>|&nbsp;|<p>&nbsp;<p>)+$}i', '',$request->getPost('reply'));
                $mp->updated_at = date("Y-m-d H:i:s");
                if($mp->save()){
                    $map = Mapcomments::findFirst("id='$mp->mapcomment_id'");
                    $map->updated_at = date("Y-m-d H:i:s");
                    if($map->save()){
                        $data = array("success"=>"Comment has been successfully edit.");
                    }else {
                        $data = array("error"=>"An error occurred, please try again later 3");
                    }
                }else {
                    $data = array("error"=>"An error occurred, please try again later 2");
                }
            }else {
                $data = array("error"=>"An error occurred, please try again later 1");
            }
            echo json_encode($data);
        }
    }
}
