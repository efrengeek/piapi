<?php

namespace Controllers;

use \Models\Agents as Agents;
use \Models\Users as Users;
use \Models\Maps as Maps;
use \Models\Markers as Markers;
use \Models\Markerpics as Markerpics;
use \Models\Mapcomments as Mapcomments;
use \Models\Commentreply as Commentreply;
use \Models\Mapcategories as Mapcategories;
use \Models\Mapcats as Mapcats;
use \Models\Maptags as Maptags;
use \Models\Tags as Tags;
use \Models\Mapnews as Mapnews;
use \Models\Mapgallery as Mapgallery;
use \Controllers\ControllerBase as CB;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;
use \Phalcon\Filter as Filter;

class MapController extends \Phalcon\Mvc\Controller {

    /**
     * @SWG\Get(
     *     path="/map/titleexist/{title}",
     *     summary="Check if Map title exists",
     *     tags={"Maps Mission"},
     *     description="Checking title exists from the database, we are avoiding the same mission title",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Check if your bearer token is still valid",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="title",
     *         in="path",
     *         description="Title String",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="Invalid tag value",
     *     )
     * )
     *
     */

    public function titleExistAction($title) {
        if(!empty($title)) {
            $map = Maps::findFirst('title="' . $title . '"');
            if ($map) {
                echo json_encode(array('exists' => 'true'));
            } else {
                echo json_encode(array('exists' => 'false'));
            }
        }
    }

    public function getMaps($num, $off, $status, $cat) {

        $joinCat = '';
        $whereCat = '';
        $fieldCat = '';
        $category = '';
        if($cat != "null"){
            $joinCat = ' INNER JOIN mapcats ON mapcats.mapid = maps.id INNER JOIN mapcategories ON mapcats.catid = mapcategories.id ';
            $whereCat = ' AND mapcategories.catslugs = "'.$cat.'" ';
            $fieldCat = ' ,mapcategories.cattitle ';

            $category = Mapcategories::findFirst("catslugs='".$cat."'");

        }

      $app = new CB();
      $offsetfinal = ($off * 10) - 10;

      $sql = "SELECT agents.first_name, agents.last_name, maps.id as mapid,maps.*, markers.long, markers.lat, markers.pin $fieldCat  FROM maps INNER JOIN agents ON maps.agent = agents.id INNER JOIN markers ON markers.map_id = maps.id $joinCat  WHERE markers.pin = 1 AND maps.status=1 $whereCat ";
      $sqlCount = "SELECT COUNT(*)  FROM maps INNER JOIN agents ON maps.agent = agents.id INNER JOIN markers ON maps.id = markers.map_id $joinCat WHERE markers.pin =1 AND maps.status=1 $whereCat ";
      $sqlStatus = '';

      if ($status != 'null' && $status != 'undefined') {
          if($status == 'FEATURED'){
              $sqlStatus = " AND maps.featured = '1' ";
          }else if($status == 'GREATEST'){
              $sqlStatus = " AND maps.greatest = '1' ";
          }else if($status == 'ALL'){
              $sqlStatus = '';
          }
      }

      $sql .= $sqlStatus;
      $sqlCount .= $sqlStatus;

      if ($offsetfinal < 0) {
          $offsetfinal = 0;
      }

      if($status == 'ALL'){
          $sql .= " ORDER BY maps.created_at DESC ";
      }else if($status == 'POPULAR'){
          $sql .= " ORDER BY maps.views DESC ";
      }else{
          $sql .= " ORDER BY maps.created_at DESC ";
      }

      $sql .= " LIMIT " . $offsetfinal . ",10";

      $maps = $app->dbSelect($sql);
      $totalreportdirty = $app->dbSelect($sqlCount);

      foreach($maps as $key=>$value){
          $sql = "SELECT COUNT(*) FROM mapcomments WHERE map_id='". $value['id'] ."'";

          $commentcount = $app->dbSelect($sql);
          $maps[$key]['comments'] = $commentcount[0]["COUNT(*)"];

          $sql2 = "SELECT * FROM markers WHERE markers.map_id = '".$maps[$key]['mapid']."'";
          $markers = $app->dbSelect($sql2);

          $maps[$key]['markers'] = $markers;
      }

      echo json_encode(array('data' => $maps, 'index' => $off, 'total_items' => $totalreportdirty[0]["COUNT(*)"], 'category' => $category));
    }

    public function uploadPicsAction() {
        $request = new \Phalcon\Http\Request();
            //Check if the user has uploaded files
            if ($request->hasFiles() == true) {
                    //Print the real file names and their sizes
                    foreach ($request->getUploadedFiles() as $file){
                            echo $file->getName(), " ", $file->getSize(), "\n";
                            $file->moveTo('files/' . $file->getName());
                    }
            }
    }

    public function saveMapMarkerAction(){
        $request = new \Phalcon\Http\Request();

        if ($request->isPost() == true) {
            try{

            $transactionManager = new TransactionManager();

            $transaction = $transactionManager->get();

            $maps = new Maps();
            $maps->setTransaction($transaction);
            $guid = new \Utilities\Guid\Guid();
            $maps->id = $guid->GUID();

             $maps->title = $request->getPost('title');
                $slugs = '';
                if($request->getPost('title') == 'Mission Title'){
                    $slugs = strtolower(str_replace(' ','-',$request->getPost('title'))) . " " . time();
                }else{
                    $slugs = strtolower(str_replace(' ','-',$request->getPost('title')));
                }
             $maps->mapslugs = $slugs;
             $maps->agent = $request->getPost('userid');
             $maps->description = nl2br($request->getPost('description'));
             $maps->status=1;
             $maps->hide_agent = ($request->getPost('showauthor') ? 1 : 0);
             $maps->cover = $request->getPost('cover');
             $maps->coverType = $request->getPost('coverType');
             $maps->created_at = date("Y-m-d H:i:s");
             $maps->updated_at = date("Y-m-d H:i:s");
             $maps->views = 0;
             if ($maps->save() == false) {
                  $array = [];
                  foreach ($maps->getMessages() as $message) {
                    $array[] = $message;
                  }
             }

             $mg = new Mapgallery();
             $mg->setTransaction($transaction);
             $guid = new \Utilities\Guid\Guid();
             $mg->assign(array(
                'id' => $guid->GUID(),
                'coverType' => $request->getPost('coverType'),
                'cover' => $request->getPost('cover'),
                'map_id' => $maps->id
             ));

             if ($mg->save() == false) {
                  $array = [];
                  foreach ($maps->getMessages() as $message) {
                    $array[] = $message;
                  }
             }
             
                //Saving Categories
                $cat = $request->getPost('category');
                foreach($cat as $c => $t){
                    $category = new Mapcats();
                    $category->setTransaction($transaction);
                    $category->mapid = $maps->id;
                    $category->catid = $t['id'];
                    $category->created_at = date("Y-m-d H:i:s");
                    $category->updated_at = date("Y-m-d H:i:s");
                    if ($category->save() == false) {
                        $array = [];
                        foreach ($maps->getMessages() as $message) {
                            $array[] = $message;
                        }
                    }
                }

                $tag = $request->getPost('tags');
                foreach($tag as $tg){
                    $find = Tags::findFirst('tag="'.strtolower($tg).'"');
                    $id = $find->id;
                    if(!$find){
                        $tags = new Tags();
                        $tags->setTransaction($transaction);
                        $tags->tag = strtolower($tg);
                        $tags->tagslugs = strtolower(str_replace(' ','-',$tg));
                        $tags->created_at = date("Y-m-d H:i:s");
                        $tags->updated_at = date("Y-m-d H:i:s");

                        if ($tags->save() == false) {
                            $array = [];
                            foreach ($tags->getMessages() as $message) {
                                $array[] = $message;
                            }
                        }else{
                            $id = $tags->id;
                        }
                    }

                    $maptags = new Maptags();
                    $maptags->setTransaction($transaction);
                    $maptags->mapid = $maps->id;
                    $maptags->tagid = $id;
                    $maptags->created_at = date("Y-m-d H:i:s");
                    $maptags->updated_at = date("Y-m-d H:i:s");

                    if ($maptags->save() == false) {
                        $array = [];
                        foreach ($maptags->getMessages() as $message) {
                            $array[] = $message;
                        }
                    }
                }

             $mk = $request->getPost('markersInfo');
             foreach($mk as $m => $k) {
                 $markers = new Markers();
                 $markers->setTransaction($transaction);
                 //$markers->id = $guid->GUID();
                 $markers->id = $k['idKey'];
                 $markers->map_id = $maps->id;
                 $markers->agent = $request->getPost('userid');
                 $markers->description = nl2br($k['description']);
                 $markers->video = $k['videolink'];
                 $markers->long = $k['longitude'];
                 $markers->lat = $k['latitude'];
                 $markers->kingpin = $k['kingpin'];
                 $markers->created_at = date("Y-m-d H:i:s");
                 $markers->updated_at = date("Y-m-d H:i:s");
                 $markers->hide_agent = ($request->getPost('showauthor') ? 1 : 0);
                 $markers->status = 1;
                 $markers->views = 0;
                 if ($m == 0) {
                     $markers->pin = 1;
                 } else {
                     $markers->pin = 0;
                 }
                 if ($markers->save() == false) {
                     $array = [];
                     foreach ($markers->getMessages() as $message) {
                         $array[] = $message;
                     }
                     var_dump($array);

                     $transaction->rollback('Cannot save markers.');
                     die( json_encode(array('401' => $array)));
                 }else {
                    $pincomment = new Mapcomments();
                    $guid = new \Utilities\Guid\Guid();
                    $pincomment->id = $guid->GUID();
                    $pincomment->comment = nl2br($k['description']);
                    $pincomment->map_id = $maps->id;
                    $pincomment->agent = $request->getPost('userid');
                    $pincomment->created_at = $markers->created_at;
                    $pincomment->updated_at = $markers->updated_at;
                    $pincomment->type = "pin";
                    $pincomment->marker_id = $k['idKey'];
                    $pincomment->status = 1;
                    $pincomment->superagent = 0;
                    if($pincomment->save() == false){
                         $array = [];
                         foreach ($pincomment->getMessages() as $message) {
                             $array[] = $message;
                         }
                         var_dump($array);

                         $transaction->rollback('Cannot save markers.');
                         die( json_encode(array('401' => $array)));
                    }
                 }
             }
                 $transaction->commit();

             }catch(\Phalcon\Mvc\Model\Transaction\Failed $e) {
                  die( json_encode(array('401' => $e->getMessage())) );
              }

            $response['200'] = "Success.";
            $response['data'] = array('id' => $maps->id, 'slugs'=> $maps->mapslugs);
            die(json_encode($response));
        }
    }

    public function savepinimageAction(){
        $request = new \Phalcon\Http\Request();
        if($request->isPost()){
            $mp = new Markerpics();
            $guid = new \Utilities\Guid\Guid();
            $mp->id = $guid->GUID();
            $mp->filename = $request->getPost('file');
            $mp->created_at = date('Y-m-d H:i:s');
            $mp->updated_at = date('Y-m-d H:i:s');
            $mp->marker_id = $request->getPost('id');
            $mp->save();
        }
    }

    public function getpinsAction($title,$agentid) {
        $agent = null;
        $app = new CB();
        $commentreply = [];
        $repcount = [];
        $comcount = 0;
        $pinimages = [];
        $map = [];

        //map
        if($title != "null"){
          $map = Maps::findFirst("mapslugs='" . $title . "'");
          if($map){
              $map->views = $map->views + 1;
              $map->save();
          }

          //agent
          $agent = Agents::findFirst("id='" . $map->agent . "'");

          //markers
          $sql = "SELECT markers.*, agents.username, agents.profile_pic_name FROM markers INNER JOIN agents ON markers.agent=agents.id WHERE map_id='$map->id' ORDER BY pin DESC";
          $marker = $app->dbSelect($sql);

          //mapcount
          $sql = "SELECT COUNT(*) FROM maps WHERE agent='$map->agent' AND status=1";
          $mapcount = $app->dbSelect($sql);
          //actioncount
          $sql = "SELECT COUNT(*) FROM markers WHERE agent='$map->agent' AND status=1";
          $actioncount = $app->dbSelect($sql);

          //comments
          $sql = "SELECT mapcomments.*, agents.first_name, agents.last_name, agents.username,agents.profile_pic_name, markers.hide_agent FROM mapcomments LEFT JOIN agents ON mapcomments.agent=agents.id LEFT JOIN markers ON mapcomments.marker_id=markers.id WHERE mapcomments.map_id='$map->id' AND mapcomments.status=1 ORDER BY mapcomments.updated_at DESC LIMIT 4";
          $comments = $app->dbSelect($sql);

          // $pinimages = array();
          foreach($comments as $key=> $value){
            if($value['type'] == 'pin'){
              $sql = "SELECT * FROM markerpics WHERE marker_id='" . $value['marker_id'] . "'";
              foreach($app->dbSelect($sql) as $key=>$value){
                array_push($pinimages, $value);
              }
            }
          }

          //comments count
          $sql = "SELECT COUNT(*) FROM mapcomments INNER JOIN agents ON mapcomments.agent=agents.id WHERE map_id='$map->id' ORDER BY mapcomments.updated_at DESC";
          $comcount = $app->dbSelect($sql)[0]["COUNT(*)"];

          foreach ($comments as $key => $value) {
            $sql = "SELECT COUNT(*) FROM commentreply INNER JOIN agents ON commentreply.agent=agents.id WHERE mapcomment_id='" . $value['id']. "' ORDER BY commentreply.created_at DESC";
            array_push($repcount, array( "count"=>$app->dbSelect($sql)[0]["COUNT(*)"], "id"=>$value['id'], "loaded"=>2));
            $comments[$key]['showedit'] = false;

            $sql = "SELECT commentreply.*, 
              IF(commentreply.agent != 'superagent', agents.first_name, 'Superagent') first_name, 
              IF(commentreply.agent != 'superagent', agents.last_name, '') as last_name,
              agents.username,
              agents.profile_pic_name FROM commentreply LEFT JOIN agents ON commentreply.agent=agents.id WHERE mapcomment_id='" . $value['id']. "' ORDER BY commentreply.created_at ASC LIMIT 2 OFFSET " . ($app->dbSelect($sql)[0]["COUNT(*)"] > 2 ? $app->dbSelect($sql)[0]["COUNT(*)"] - 2 : 0);
            $res = $app->dbSelect($sql);
            foreach($res as $key => $value) {
                $res[$key]['showedit'] = false;
                array_push($commentreply, $res[$key]);
            }
          }

          $sql = "SELECT mapcategories.cattitle, mapcategories.catslugs FROM mapcategories INNER JOIN mapcats ON mapcategories.id=mapcats.catid WHERE mapcats.mapid='$map->id'";
          $categories = $app->dbSelect($sql);

          $sql = "SELECT tags.tag, tags.tagslugs FROM tags INNER JOIN maptags ON tags.id=maptags.tagid WHERE maptags.mapid='$map->id'";
          $tags = $app->dbSelect($sql);

          $sql = "SELECT * FROM mapnews WHERE mapid='$map->id' ORDER BY created_at DESC LIMIT 3";
          $mapnews = $app->dbSelect($sql);

          echo json_encode(array('map' => $map, 'agent'=>$agent, 'markers'=>$marker, 'pinimages'=>$pinimages, 'mapcount'=>$mapcount[0]["COUNT(*)"], 'actioncount'=>$actioncount[0]["COUNT(*)"], 'comments' => $comments, 'commentcount' =>$comcount, 'reply' => $commentreply, 'replycount' => $repcount, 'categories' => $categories, 'tags' => $tags, 'mapnews' => $mapnews), JSON_NUMERIC_CHECK);
        }else {
           $count = $app->dbSelect("SELECT COUNT(*) FROM maps WHERE agent='$agentid'")[0]["COUNT(*)"];
          //markers
          $sql = "SELECT markers.*, agents.username, agents.profile_pic_name FROM markers INNER JOIN agents ON markers.agent=agents.id WHERE markers.agent='$agentid' ORDER BY pin DESC";
          $marker = $app->dbSelect($sql);

          $agent = Agents::findFirst("id='$agentid'");
          echo json_encode(array('markers' => $marker, 'agent' => $agent, 'mapcount' => $count ), JSON_NUMERIC_CHECK);
        }
    }

    public function getpinAction($id) {
      $app = new CB();
      $sql = "SELECT markers.*, agents.first_name, agents.last_name, agents.username, agents.profile_pic_name FROM markers INNER JOIN agents ON markers.agent=agents.id WHERE markers.id='$id' ORDER BY pin DESC";
      $marker = $app->dbSelect($sql)[0];
      if($marker){
        $comments = $app->dbSelect("SELECT commentreply.*, agents.first_name, agents.last_name, agents.username, agents.profile_pic_name FROM commentreply LEFT JOIN agents ON commentreply.agent=agents.id WHERE marker_id='$id' ORDER BY commentreply.updated_at DESC");
        foreach ($comments as $key => $value) {
            $comments[$key]['showedit'] = false;
        }
        $images = Markerpics::find("marker_id='$id'");
        $agent = Agents::findFirst("id='" . $marker['agent'] . "'");
        $map = Maps::findFirst("id='" . $marker['map_id'] . "'");
        echo json_encode(array('pin' => $marker, 'comments' => $comments, 'images' => $images->toArray(), 'agent' => $agent, 'map' => $map));
      }
    }

    public function savepinAction(){
        $request = new \Phalcon\Http\Request();
        if($request->isPost()){
            $markers = new Markers();
            $markers->id = $request->getPost('idKey');
            $markers->map_id = $request->getPost('map_id');
            $markers->agent = $request->getPost('userid');
            $markers->description = nl2br($request->getPost('description'));
            $markers->video = $request->getPost('videolink');
            $markers->long = $request->getPost('longitude');
            $markers->lat = $request->getPost('latitude');
            $markers->created_at = date("Y-m-d H:i:s");
            $markers->updated_at = date("Y-m-d H:i:s");

            $m = Maps::findFirst("id='" . $request->getPost('map_id') . "'");

            $markers->status = ($m->agent == $request->getPost('userid') ? 1 : ($m->approvepins == 0 ? 1 : 0) );

            //$markers->status = 1;
            $markers->pin = 0;
            $markers->hide_agent = ($request->getPost("showauthor") == 'true' ? 1 : 0);
            $markers->views = 0;

            if($markers->save()){
                $comment = new Mapcomments();
                $guid = new \Utilities\Guid\Guid();
                $comment->id = $guid->GUID();
                $comment->comment = nl2br($request->getPost('description'));
                $comment->map_id = $request->getPost('map_id');
                $comment->agent = $request->getPost('userid');
                $comment->created_at = date("Y-m-d H:i:s");
                $comment->updated_at = date("Y-m-d H:i:s");
                $comment->marker_id = $request->getPost('idKey');
                $comment->type = "pin";
                $comment->status = $markers->status;
                $comment->superagent = 0;

                if($comment->save()){
                  $data = array('success' => 'Comment has been successfully added.');
                }else {
                  $data = array('error' => 'An error occurred please try again later.');
                }
            }else {
                echo "error";
            }

            echo json_encode($data);
        }
    }

    public function deletepinAction($id) {
        if($id){
            $markers = Markers::findFirst("id='$id'");

            if($markers->pin == 1){
              $m = Markers::findFirst(array("id!='$id' AND map_id='$markers->map_id'","order" => 'created_at'));
              if($m){
                $m->pin = 1;
                if(!$m->save()){
                  echo json_encode(array('error' => 'An error occurred please try again later.'));
                }
              }else {
                  echo json_encode(array('error' => 'An error occurred please try again later.'));
              }
            }

            if($markers->delete()){
              $mp = Mapcomments::find("marker_id='$id'");
              $mp->delete();
              $mp = Commentreply::find("marker_id='$id'");
              $mp->delete();
              echo json_encode(array('success' => 'Action pin has been successfully deleted.'));
            }else {
              echo json_encode(array('error' => 'An error occurred please try again later.'));
            }
        }else {
            echo json_encode(array('error' => 'No id'));
        }
    }

    public function pinviewAction($id){
        $marker = Markers::findFirst("id='$id'");
        $views = $marker->views;
        $marker->views = $marker->views + 1;
        if($marker->save()){
            echo json_encode($views, JSON_NUMERIC_CHECK);
        }
    }

    public function setkingpinAction($id, $mapid){
        $marker = Markers::findFirst("pin='1' AND map_id='$mapid'");
        $marker->pin = 0;
        if($marker->save()){
            $marker = Markers::findFirst("id='" . $id . "'");
            $marker->pin = 1;
            $map = Maps::findFirst("id='$mapid'");
            $map->agent = $marker->agent;
            if($marker->save() && $map->save()){
                $data = array('success' => 'Pin has been successfully set to king pin');
            }
        }else {
            $data = array('error' => 'An error occurred please try again later');
        }
        echo json_encode($data);
    }

    public function getpinimagesAction($id) {
        $pinimages = Markerpics::find("marker_id='" . $id . "'");
        if($pinimages){
            echo json_encode($pinimages->toArray(), JSON_NUMERIC_CHECK);
        }
    }

    public function updatePinDescAction($id, $desc) {
        $marker = Markers::findFirst("id='" . $id . "'");
        if($marker){
            $marker->description = nl2br($desc);
            if($marker->save()){
                $mp = Mapcomments::findFirst("marker_id='$id'");
                if($mp){
                  $mp->updated_at = date("Y-m-d H:i:s");
                  $mp->comment = nl2br($desc);
                  if($mp->save()){
                    $data = array('success' => "Description successfully updated");
                  }else {
                    $data = array('error' => "An error occured please try again later.");
                  }
                }else {
                  $data = array('error' => "An error occured please try again later.");
                }
            }else {
                $data = array('error' => "An error occured please try again later.");
            }
        }else {
                $data = array('error' => "An error occured please try again later.");
        }
        echo json_encode($data);
    }

    public function updatemapinfoAction($id) {
        $request = new \Phalcon\Http\Request();
        if($request->isPost()){
          try{

            $transactionManager = new TransactionManager();

            $transaction = $transactionManager->get();

            $title = $request->getPost('title');
            $description = str_replace('<br />', '', $request->getPost('description'));

            $map = Maps::findFirst("id='" . $id ."'");
            $map->setTransaction($transaction);

            $tag = $request->getPost('tags');

            $conditions = 'mapid="' . $map->id . '"';
            $dlttags = Maptags::find(array($conditions));
            if ($dlttags) {
                foreach($dlttags as $dlttags){
                    if ($dlttags->delete()) {
                        $data = array('success' => 'Categories Deleted');
                    }
                }
            }

            foreach($tag as $tg){
                $find = Tags::findFirst('tag="'.strtolower($tg).'"');
                $id = $find->id;
                if(!$find){
                    $tags = new Tags();
                    $tags->setTransaction($transaction);
                    $tags->tag = strtolower($tg);
                    $tags->tagslugs = strtolower(str_replace(' ','-',$tg));
                    $tags->created_at = date("Y-m-d H:i:s");
                    $tags->updated_at = date("Y-m-d H:i:s");

                    if ($tags->save() == false) {
                        $array = [];
                        foreach ($tags->getMessages() as $message) {
                            $array[] = $message;
                        }
                    }else{
                        $id = $tags->id;
                    }
                }

                $mt = Maptags::findFirst("mapid='$map->id' AND tagid='$id'");
                if(!$mt){
                  $maptags = new Maptags();
                  $maptags->setTransaction($transaction);
                  $maptags->mapid = $map->id;
                  $maptags->tagid = $id;
                  $maptags->created_at = date("Y-m-d H:i:s");
                  $maptags->updated_at = date("Y-m-d H:i:s");

                  if ($maptags->save() == false) {
                    $array = [];
                    foreach ($maptags->getMessages() as $message) {
                        $array[] = $message;
                    }
                  }
                }
            }

            //Saving Categories
            $cat = $request->getPost('category');

            $conditions = 'mapid="' . $map->id . '"';
            $dltcat = Mapcats::find(array($conditions));
            if ($dltcat) {
                foreach($dltcat as $dltcat){
                    if ($dltcat->delete()) {
                        $data = array('success' => 'Categories Deleted');
                    }
                }
            }

            foreach($cat as $c => $t){
              $mp = Mapcats::findFirst("mapid='$map->id' AND catid='" . $t['id'] . "'");
              if(!$mp){
                $category = new Mapcats();
                $category->setTransaction($transaction);
                $category->mapid = $map->id;
                $category->catid = $t['id'];
                $category->created_at = date("Y-m-d H:i:s");
                $category->updated_at = date("Y-m-d H:i:s");
                if ($category->save() == false) {
                    $array = [];
                    foreach ($maps->getMessages() as $message) {
                        $array[] = $message;
                    }
                }
              }
            }

            if($map){
                $map->title = $title;
                $map->description = nl2br($description);
                if($map->save()){
                    $data = array('success' => 'Map has been successfully edited.' );
                }else {
                    $array = [];
                    foreach ($maps->getMessages() as $message) {
                      $array[] = $message;
                    }
                }
            }else {
                $array = [];
                foreach ($maps->getMessages() as $message) {
                  $array[] = $message;
                }
            }
            $transaction->commit();
            echo json_encode($data);
          }catch(\Phalcon\Mvc\Model\Transaction\Failed $e) {
            die( json_encode(array('401' => $e->getMessage())) );
          }
        }
    }
    public function updatecoverAction($id, $type) {
      $request = new \Phalcon\Http\Request();
      if($request->isPost()){
        $map = Maps::findFirst("id='$id'");
        if($map){
          $map->coverType = $type;
          $map->cover = $request->getPost('data');
          $map->updated_at = date("Y-m-d H:i:s");
          if($map->save()){
            $data = array('success' => 'Map cover has been successfully updated.' );
          }else {
            $data = array('error' => 'An error occured, please try again later.' );
          }
        }else {
          $data = array('error' => 'An error occured, please try again later.' );
        }
        echo json_encode($data);
      }
    }
    public function deletepinimgAction($id) {
      $mp = Markerpics::findFirst("id='$id'");
      if($mp->delete()){
        $data = array('success' => 'Image has been successfully deleted.' );
      }else {
        $data = array('error' => 'An error occured, please try again later.' );
      }
      echo json_encode($data);
    }

    public function updatevideoAction($id) {
      $request = new \Phalcon\Http\Request();
      if($request->isPost()){
        $marker = Markers::findFirst("id='$id'");
        if($marker){
          $marker->video = $request->getPost('video');
          if($marker->save()){
            $data = array('success' => 'Pin video has been successfully edited.' );
          }else {
            $data = array('error' => 'An error occured, please try again later.' );
          }
        }else {
          $data = array('error' => 'An error occured, please try again later.' );
        }
        echo json_encode($data);
      }
    }

    public function hideagentAction($id, $hideagent) {
      $marker = Markers::findFirst("id='$id'");
      if($marker){
        $marker->hide_agent = $hideagent;
        if($marker->save()){
          $data = array('success' => 'success' );
        }else {
          $data = array('error' => 'error' );
        }
      }else {
        $data = array('error' => 'error' );
      }
      echo json_encode($data);
    }

    public function approvepinsAction($id, $val) {
        $mp = Maps::findFirst("id='$id'");
        if($mp){
            $mp->approvepins = $val;
            if($mp->save()){
                $data = array('success' => 'success' );
            }else {
                $data = array('error' => 'error' );
            }
        }else {
            $data = array('error' => 'error' );
        }
        echo json_encode($data);
    }

    public function maphideagentAction($id, $hideagent) {
      $mp = Maps::findFirst("id='$id'");
      if($mp){
        $mp->hide_agent = $hideagent;
        if($mp->save()){
          $data = array('success' => 'success' );
        }else {
          $data = array('error' => 'error' );
        }
      }else {
        $data = array('error' => 'error' );
      }
      echo json_encode($data);
    }

    public function missionslistAction($num, $page, $keyword, $status)
    {
        $app = new CB();

        // offsetting
        $offsetfinal = ($page * 10) - 10;
        $sql = 'SELECT agents.first_name, agents.last_name, agents.email, agents.id as agentid, maps.title, maps.description, maps.created_at, maps.updated_at, maps.id as mapid, maps.featured, maps.greatest, maps.views, maps.status, maps.mapslugs FROM maps INNER JOIN agents ON maps.agent = agents.id';
        $sqlCount = 'SELECT COUNT(*) FROM maps INNER JOIN agents ON maps.agent = agents.id';
        $sqlWhere = '';
        $sqlStatus = '';
        if ($keyword != 'null' && $keyword != 'undefined') {
          $keyword = addslashes($keyword);
          $sqlWhere = " WHERE agents.first_name LIKE '%" . $keyword . "%' OR agents.last_name LIKE '%" . $keyword . "%' OR agents.id LIKE '%" . $keyword . "%' OR maps.title LIKE '%" . $keyword . "%' OR maps.description LIKE '%" . $keyword . "%' OR maps.id LIKE '%" . $keyword . "%' ";
        }

        if ($status != 'null' && $status != 'undefined') {
            if($status == 'FEATURED'){
                $sqlStatus = " maps.featured = '1' ";
            }else if($status == 'GREATEST'){
                $sqlStatus = " maps.greatest = '1' ";
            }else if($status == 'ALL'){
                $sqlStatus = '';
            }
        }

        // $sql .= $sqlWhere != '' ? $sqlWhere." ".$sqlStatus : !empty($sqlStatus) ? " WHERE " . $sqlStatus : '';
        // $sqlCount .= !empty($sqlWhere) ? $sqlWhere." ".$sqlStatus : !empty($sqlStatus) ? " WHERE " . $sqlStatus : '';
        
        if($sqlWhere != '') {
          if($sqlStatus != ''){
            $sql .= $sqlWhere." AND ".$sqlStatus;
            $sqlCount .= $sqlWhere." AND ".$sqlStatus;
          }else {
            $sql .= $sqlWhere;
            $sqlCount .= $sqlWhere;
          }
        }else {
          if($sqlStatus !=''){
            $sql .= " WHERE " .$sqlStatus;
            $sqlCount .= " WHERE " .$sqlStatus;
          }
        }

        if ($offsetfinal < 0) {
            $offsetfinal = 0;
        }

        if($status == 'LATEST'){
            $sql .= " ORDER BY maps.created_at DESC ";
        }else if($status == 'POPULAR'){
            $sql .= " ORDER BY maps.views DESC ";
        }else{
            $sql .= " ORDER BY maps.updated_at DESC ";
        }
        $sql .= " LIMIT " . $offsetfinal . ",10";

        // getting the query
        $searchresult = $app->dbSelect($sql);

        $totalreportdirty = $app->dbSelect($sqlCount);

        echo json_encode(array('data' => $searchresult, 'index' => $page, 'total_items' => $totalreportdirty[0]["COUNT(*)"]));
    }

    public function missionfeaturedAction($val, $id, $type){
        $data = array();
        $map = Maps::findFirst('id="' . $id . '"');
        if($type == 'f'){
            $map->featured = $val == "true" ? 1 : 0;
        }else if($type == 'g'){
            $map->greatest = $val == "true" ? 1 : 0;
        }
        $map->updated_at = date('Y-m-d H:i:s');
        if (!$map->save()) {
            $data['error'] = "Something went wrong when adding to featured missions, please try again.";
        } else {
            $data['success'] = "Success";
        }

        echo json_encode($data);
    }

    public function missionindexAction(){
        $app = new CB();
        $sql = 'SELECT
            IF(maps.hide_agent=1,agents.first_name,"Anonymous") as first_name ,
            IF(maps.hide_agent=1,agents.last_name,"") as last_name,
            agents.email,
            agents.id as agentid,
            agents.username,
            agents.profile_pic_name,
            maps.title,
            maps.description,
            maps.created_at,
            maps.updated_at,
            maps.id as mapid,
            maps.featured,
            maps.greatest,
            maps.views,
            maps.cover,
            maps.coverType,
            maps.status,
            maps.mapslugs,
            maps.hide_agent,
            count(*) as actions,
            COALESCE(mapcom.countComments,0) AS countComments,
            COALESCE(commentrep.countRepComments,0) AS countRepComments

              FROM

              maps INNER JOIN agents ON maps.agent = agents.id INNER JOIN markers ON maps.id = markers.map_id

                LEFT JOIN (SELECT COUNT(*) AS countComments, map_id
                        FROM mapcomments WHERE type != "pin"
                        GROUP BY map_id) AS mapcom ON mapcom.map_id = maps.id

                LEFT JOIN (SELECT COUNT(*) AS countRepComments, map_id
                        FROM commentreply
                        GROUP BY map_id) AS commentrep ON commentrep.map_id = maps.id

              ';

        $sqlFeatured = $sql . " WHERE maps.featured = '1' AND maps.status=1 GROUP BY maps.id LIMIT 4 ";
        $searchResultFeatured = $app->dbSelect($sqlFeatured);

        $sqlGreatest = $sql . " WHERE maps.greatest = '1' AND maps.status=1 GROUP BY maps.id LIMIT 3";
        $searchResultGreatest= $app->dbSelect($sqlGreatest);

        $sqlLatest = $sql . " WHERE maps.status=1 GROUP BY maps.id ORDER BY created_at DESC LIMIT 3";
        $searchResultLatest = $app->dbSelect($sqlLatest);

        $sqlPopular = $sql . " WHERE maps.status=1 GROUP BY maps.id ORDER BY maps.views DESC LIMIT 3";
        $searchResultPopular = $app->dbSelect($sqlPopular);


        //Getting Categories
        $sqlMissionCategories = 'SELECT mapcategories.*, COUNT(mapcats.id) AS post_count FROM mapcategories LEFT JOIN mapcats ON mapcategories.id = mapcats.catid GROUP BY mapcategories.id ORDER BY post_count DESC LIMIT 4';
        $searchResultCategories = $app->dbSelect($sqlMissionCategories);

        $sql = "SELECT * FROM news WHERE status=1 ORDER BY created_at DESC LIMIT 3";
        $news = $app->dbSelect($sql);

        die(json_encode(array(
            'featured'=> $searchResultFeatured,
            'greatest'=> $searchResultGreatest,
            'latest'=> $searchResultLatest,
            'popular'=> $searchResultPopular,
            'categories' => $searchResultCategories,
            'news' => $news
        )));

    }

    public function addCategoryAction(){
        $request = new \Phalcon\Http\Request();
        $guid = new \Utilities\Guid\Guid();

        $cat = Mapcategories::findFirst('cattitle="'.$request->getPost('title').'"');

        if(!$cat){

            $mc = new Mapcategories();
            $mc->id = $guid->GUID();
            $mc->cattitle = $request->getPost('title');
            $mc->catslugs = strtolower(str_replace(' ','-',$request->getPost('title')));
            $mc->catdescription = $request->getPost('description');
            $mc->created_at = date("Y-m-d H:i:s");
            $mc->updated_at = date("Y-m-d H:i:s");

            if($mc->save() == false){
                $array = [];
                foreach ($mc->getMessages() as $message) {
                    $array[] = $message;
                }

                $mc->rollback('Cannot save markers.');
                die( json_encode(array('401' => $array)));
            }else{
                die(json_encode(array('200'=>'Success')));
            }
        }else{
            die( json_encode(array('error' => 'Title already taken.')));
        }

    }

    public function updateCategoryAction(){
        $request = new \Phalcon\Http\Request();

        $cat = Mapcategories::findFirst('id != "'.$request->getPost('id').'" AND cattitle="'.$request->getPost('title').'"');

        if(!$cat){

            $mc = Mapcategories::findFirst('id = "'.$request->getPost('id').'"');

            if($mc){
                $mc->cattitle = $request->getPost('title');
                $mc->catslugs = strtolower(str_replace(' ','-',$request->getPost('title')));
                $mc->catdescription = $request->getPost('description');
                $mc->updated_at = date("Y-m-d H:i:s");

                if($mc->save() == false){
                    $array = [];
                    foreach ($mc->getMessages() as $message) {
                        $array[] = $message;
                    }

                    $mc->rollback('Cannot save markers.');
                    die( json_encode(array('401' => $array)));
                }else{
                    die(json_encode(array('200'=>'Success')));
                }
            }else{
                die( json_encode(array('404' => 'Not found')));
            }
        }else{
            die( json_encode(array('error' => 'Title already taken.')));
        }
    }

    public function listCategoryAction($num, $page, $keyword){
        $app = new CB();

        // offsetting
        $offsetfinal = ($page * 10) - 10;
        $sql = 'SELECT * FROM mapcategories';
        $sqlCount = 'SELECT COUNT(*) FROM mapcategories';
        $sqlWhere = '';
        if ($keyword != 'null' && $keyword != 'undefined') {
          $keyword = addslashes($keyword);
          $sqlWhere = " WHERE cattitle LIKE '%" . $keyword . "%' OR catdescription LIKE '%" . $keyword . "'";
        }

        $sql .= $sqlWhere;
        $sqlCount .= $sqlWhere;

        if ($offsetfinal < 0) {
            $offsetfinal = 0;
        }
        $sql .= " ORDER BY created_at DESC ";

        $sql .= " LIMIT " . $offsetfinal . ",10";
        // getting the query
        $searchresult = $app->dbSelect($sql);

        $totalreportdirty = $app->dbSelect($sqlCount);

        echo json_encode(array('data' => $searchresult, 'index' => $page, 'total_items' => $totalreportdirty[0]["COUNT(*)"]));

    }

    public function deleteCategoryAction($catid){
        if($catid){
            $mc = Mapcategories::findFirst('id = "'.$catid.'"');
            if($mc->delete()){
                echo json_encode(array('200' => 'Mission Category has been deleted.'));
            }else {
                echo json_encode(array('error' => 'An error occurred please try again later.'));
            }
        }else {
            echo json_encode(array('error' => 'No id'));
        }
    }

    public function getCategoriesAction(){

        $app = new CB();

        $sql = 'SELECT mapcategories.*, COALESCE(mc.countMaps,0) AS countMaps FROM mapcategories
                                    LEFT JOIN
                                        (SELECT COUNT(*) AS countMaps, catid
                                                                FROM mapcats INNER JOIN maps ON maps.id = mapcats.mapid WHERE maps.status=1
                                                                GROUP BY catid) AS mc ON mc.catid = mapcategories.id ORDER BY countMaps DESC
                                                                ';
        $maps = $app->dbSelect($sql);

        if($maps){
            echo json_encode($maps);
        }
    }

    public function getMainNewsAction() {
      $app = new CB();
        $sql = "SELECT * FROM news WHERE status=1 ORDER BY created_at DESC LIMIT 3";
        $news = $app->dbSelect($sql);
        if($news){
          echo json_encode($news);
        }
    }

    public function getTagsAction(){
        $mc = Tags::find();
        if($mc){
            echo json_encode($mc->toArray(), JSON_NUMERIC_CHECK);
        }
    }

    public function pendingpinAction($id, $actiontype) {
      $marker = Markers::findFirst("id='$id'");
      if($marker){
        if($actiontype == "approve"){
          $marker->status = 1;
          if($marker->save()){
            $mc = Mapcomments::findFirst("marker_id='$id'");
            $mc->status = 1;
            if($mc->save()){
              $data = array('success' => 'Pin has been successfully approved.' );
            }else {
              $data = array('error' => 'An error occured, please try again later.' );
            }
          }else {
            $data = array('error' => 'An error occured, please try again later.' );
          }
        }else if($actiontype == "disapprove"){
          if($marker->delete()){
            $markerpics = Markerpics::findFirst("marker_id='$id'");
            $mc = Mapcomments::findFirst("marker_id='$id'");
            $comrep = Commentreply::findFirst("marker_id='$id'");
            if($markerpics){
              $markerpics->delete();
            }
            if($mc){
              $mc->delete();
            }
            if($comrep){
              $comrep->delete();
            }

            $data = array('success' => 'Pin has been disapproved.' );
          }else {
            $data = array('error' => 'An error occured, please try again later.' );
          }
        }
      }else {
        $data = array('error' => 'An error occured, please try again later.' );
      }
      echo json_encode($data);
    }

    public function getagentmapsAction($id) {
      $app = new CB();
      $sql = "SELECT maps.id as mapid, maps.*, markers.long, markers.lat, markers.pin  FROM maps INNER JOIN markers ON markers.map_id = maps.id WHERE markers.pin =1 AND maps.agent='$id'";
      $maps = $app->dbSelect($sql);
      foreach($maps as $key=>$value){
          $sql2 = "SELECT * FROM markers WHERE markers.map_id = '".$maps[$key]['mapid']."'";
          $sqlCount = "SELECT COUNT(*)  FROM markers WHERE markers.map_id = '".$maps[$key]['mapid']."' AND status=0";
          $maps[$key]['pendingpins'] = $app->dbSelect($sqlCount)[0]["COUNT(*)"];
          $sqlCount = "SELECT COUNT(*)  FROM mapcomments WHERE mapcomments.map_id = '".$maps[$key]['mapid']."' AND status=1";
          $maps[$key]['commentcount'] = $app->dbSelect($sqlCount)[0]["COUNT(*)"];
          $markers = $app->dbSelect($sql2);

          $maps[$key]['markers'] = $markers;
      }

      echo json_encode(array('data' => $maps));
    }

    public function getmapCatTags($id) {
      $app = new CB();
      $sql = "SELECT mapcategories.* FROM mapcategories INNER JOIN mapcats ON mapcategories.id=mapcats.catid WHERE mapcats.mapid='$id'";
      $cat = $app->dbSelect($sql);

      $app = new CB();
      $sql = "SELECT tags.* FROM tags INNER JOIN maptags ON tags.id=maptags.tagid WHERE maptags.mapid='$id'";
      $tags = $app->dbSelect($sql);

      echo json_encode(array('category' => $cat, 'tags' => $tags));
    }

    public function getpopulartagsAction(){
      $app = new CB();
      $sql = "SELECT tag, id FROM tags";
      $tags = $app->dbSelect($sql);

      foreach($tags as $key=>$value) {
        $sql = "SELECT COUNT(*) FROM maptags WHERE tagid='" . $value['id'] . "'";
        $tags[$key]['count'] = $app->dbSelect($sql)[0]["COUNT(*)"];
      }

      echo json_encode($tags);
    }

    public function searchmapAction($off, $keyword, $category, $tag) {
      $app = new CB();
      $offsetfinal = ($off * 9) - 9;
      $sql = "SELECT maps.*, 
        agents.first_name, 
        agents.last_name,
        agents.username,
        agents.email, 
        agents.profile_pic_name ,
        count(*) as actions,
        COALESCE(mapcom.countComments,0) AS countComments,
        COALESCE(commentrep.countRepComments,0) AS countRepComments

          FROM

          maps INNER JOIN agents ON maps.agent = agents.id INNER JOIN markers ON maps.id = markers.map_id

            LEFT JOIN (SELECT COUNT(*) AS countComments, map_id
                    FROM mapcomments WHERE type != 'pin'
                    GROUP BY map_id) AS mapcom ON mapcom.map_id = maps.id

            LEFT JOIN (SELECT COUNT(*) AS countRepComments, map_id
                    FROM commentreply
                    GROUP BY map_id) AS commentrep ON commentrep.map_id = maps.id";

      if($category!="null"){
        $sql.= "INNER JOIN mapcats ON maps.id=mapcats.mapid INNER JOIN mapcategories ON mapcats.catid=mapcategories.id WHERE mapcategories.cattitle='$category' ";
      }
      if($tag!="null"){
        $sql.= "INNER JOIN maptags ON maps.id=maptags.mapid INNER JOIN tags ON maptags.tagid=tags.id WHERE tags.tag='$tag' ";
      }

      if($keyword!="null"){
        $keyword = addslashes($keyword);
        $sql.= " LEFT JOIN mapcats ON maps.id=mapcats.mapid ";
        $sql.= "LEFT JOIN mapcategories ON mapcats.catid=mapcategories.id ";
        $sql.= "LEFT JOIN maptags ON maps.id=maptags.mapid ";
        $sql.= "LEFT JOIN tags ON maptags.tagid=tags.id ";
        $sql.= "WHERE ( maps.title LIKE '%".$keyword."%' OR maps.description LIKE '%".$keyword."%' OR ";
        $sql.= " ( maps.hide_agent=1 AND (agents.first_name LIKE '%".$keyword."%' OR agents.last_name LIKE '%".$keyword."%' OR agents.email LIKE '%".$keyword."%')) OR ";
        $sql.= "mapcategories.cattitle LIKE '%".$keyword."%' OR tags.tag LIKE '%".$keyword."%' )";
      }

      $sql.= "  GROUP BY maps.id ORDER BY maps.created_at DESC";
      $count =count($app->dbSelect($sql));
      $sql.= " LIMIT " . $offsetfinal . ",9";
      $maps = $app->dbSelect($sql);

      echo json_encode(array('maps'=>$maps, 'total_items' => $count));
    }

    public function changestatAction($id, $status) {
      if($status == 1){
        $action = "activated";
      }else {
        $action = "deactivated";
      }
      $map = Maps::findFirst("id='$id'");
      if($map){
        $map->status = $status;
        if($map->save()){
          $data = array('success' => 'Map has been ' . $action .' successfully.');
        }else {
          $data = array('error' => 'An error occurred please try again later.');
        }
        echo json_encode($data);
      }
    }

    public function getmapnewslistAction($id) {
      $app = new CB();
      $sql = "SELECT * FROM mapnews WHERE mapid='$id' ORDER BY created_at DESC LIMIT 3";
      $mapnews = $app->dbSelect($sql);
      echo json_encode($mapnews);
    }

    public function getnewsAction($id, $limit) {
      $app = new CB();
      $sqlcount = "SELECT COUNT(*) FROM mapnews WHERE mapid='$id'";
      $count = $app->dbSelect($sqlcount);
      $sql = "SELECT * FROM mapnews WHERE mapid='$id' ORDER BY created_at DESC LIMIT $limit";
      $mapnews = $app->dbSelect($sql);
      echo json_encode(array('news' => $mapnews, 'count' => $count[0]["COUNT(*)"]));
    }

    public function deletenewsAction($id) {
      $m = Mapnews::findFirst("id='$id'");
      if($m){
        if($m->delete()){
          $data = array('success' => 'Announcement has been succesfull deleted.');
        }else {
          $data = array('error' => 'An error occurred please try again later.');
        }
      }else {
        $data = array('error' => 'An error occurred please try again later.');
      }
      echo json_encode($data);
    }

    public function updatenewsAction($id) {
      $request = new \Phalcon\Http\Request();

      if($request->isPost()){
        $m = Mapnews::findFirst("id='$id'");
        if($m){
          $m->news = $request->getPost('news');
          $m->coverType = $request->getPost('coverType');
          $m->cover = $request->getPost('coverType') == 'image' ? $request->getPost('image') : $request->getPost('cover');
          $m->updated_at = date('Y-m-d H:i:s');
          if($m->save()){
            $data = array('success' => 'Announcement has been successfully edited.');
          }else {
            $data = array('error' => 'An error occurred please try again later.');
          }
        }else {
          $data = array('error' => 'An error occurred please try again later.');
        }
        echo json_encode($data);
      }
    }

    public function deleteAction($id) {
      try {

        Maps::find("id='$id'")->delete();
        Markers::find("map_id='$id'")->delete();
        Maptags::find("mapid='$id'")->delete();
        Mapcats::find("mapid='$id'")->delete();
        Mapcomments::find("map_id='$id'")->delete();
        Commentreply::find("map_id='$id'")->delete();
        foreach($marker as $key => $value){
          Markerpics::find("marker_id='" . $value['id'] . "'")->delete();
        } 

        echo json_encode(array('type' => 'success', 'msg' => 'Map has been successfully deleted.'));
      } catch (Exception $e) {
        echo json_encode(array('type' => 'danger', 'msg' => 'An error occurred please try again later.'));
      }
        
    }

    public function getmissionmapAction($slug) {
      $app = new CB();
      $commentreply = [];
      $repcount = [];
      $comcount = 0;
      $pinimages = [];
      $map = [];

      $map = $app->dbSelect("SELECT * FROM maps WHERE mapslugs='$slug'")[0];

      //markers
      $sql = "SELECT markers.*, agents.username, agents.profile_pic_name FROM markers INNER JOIN agents ON markers.agent=agents.id WHERE map_id='" . $map['id']. "' ORDER BY pin DESC";
      $marker = $app->dbSelect($sql);

      //comments
      $sql = "SELECT mapcomments.*, agents.first_name, agents.last_name, agents.username,agents.profile_pic_name, markers.hide_agent FROM mapcomments LEFT JOIN agents ON mapcomments.agent=agents.id LEFT JOIN markers ON mapcomments.marker_id=markers.id WHERE mapcomments.map_id='" . $map['id']. "' AND mapcomments.status=1 ORDER BY mapcomments.updated_at DESC LIMIT 4";
      $comments = $app->dbSelect($sql);

      // $pinimages = array();
      foreach($comments as $key=> $value){
        if($value['type'] == 'pin'){
          $sql = "SELECT * FROM markerpics WHERE marker_id='" . $value['marker_id'] . "'";
          foreach($app->dbSelect($sql) as $key=>$value){
            array_push($pinimages, $value);
          }
        }
      }

      //comments count
      $sql = "SELECT COUNT(*) FROM mapcomments LEFT JOIN agents ON mapcomments.agent=agents.id WHERE map_id='" . $map['id']. "' ORDER BY mapcomments.updated_at DESC";
      $comcount = $app->dbSelect($sql)[0]["COUNT(*)"];

      foreach ($comments as $key => $value) {
          $sql = "SELECT COUNT(*) FROM commentreply LEFT JOIN agents ON commentreply.agent=agents.id WHERE mapcomment_id='" . $value['id']. "' ORDER BY commentreply.created_at DESC";
          array_push($repcount, array( "count"=>$app->dbSelect($sql)[0]["COUNT(*)"], "id"=>$value['id'], "loaded"=>2));
          $comments[$key]['showedit'] = false;

          $sql = "SELECT commentreply.*, 
            IF(commentreply.agent != 'superagent', agents.first_name, 'Superagent') as first_name,
            IF(commentreply.agent != 'superagent', agents.last_name, '') as last_name,
            agents.username,
            agents.profile_pic_name FROM commentreply LEFT JOIN agents ON commentreply.agent=agents.id WHERE mapcomment_id='" . $value['id']. "' ORDER BY commentreply.created_at ASC LIMIT 2 OFFSET " . ($app->dbSelect($sql)[0]["COUNT(*)"] > 2 ? $app->dbSelect($sql)[0]["COUNT(*)"] - 2 : 0);
          $res = $app->dbSelect($sql);
          foreach($res as $key => $value) {
              $res[$key]['showedit'] = false;
              array_push($commentreply, $res[$key]);
          }
      }

      $map['categories'] = [];
      $cat = $app->dbSelect("SELECT mapcategories.id FROM mapcategories INNER JOIN mapcats ON mapcategories.id=mapcats.catid WHERE mapcats.mapid='" . $map['id']. "'");
      foreach ($cat as $key => $value) {
        array_push($map['categories'], $value['id']);
      }

      $map['tags'] = [];
      $tags = $app->dbSelect("SELECT tags.tag FROM tags INNER JOIN maptags ON tags.id=maptags.tagid WHERE maptags.mapid='" . $map['id']. "'");
      foreach ($tags as $key => $value) {
        array_push($map['tags'], $value['tag']);
      }

      $mapnews = $app->dbSelect("SELECT * FROM mapnews WHERE mapid='" . $map['id']. "' ORDER BY created_at DESC LIMIT 4");
      $tags = $app->dbSelect("SELECT tag FROM tags");
      $categories = $app->dbSelect("SELECT * FROM mapcategories");

      echo json_encode(array('map' => $map, 'markers'=>$marker, 'pinimages'=>$pinimages, 'comments' => $comments, 'commentcount' =>$comcount, 'reply' => $commentreply, 'replycount' => $repcount, 'mapnews' => $mapnews, 'categories' => $categories, 'tags' => $tags), JSON_NUMERIC_CHECK);
    }

    public function addMapCatAction() {
        $request = new \Phalcon\Http\Request();
        $guid = new \Utilities\Guid\Guid();

        $cat = Mapcategories::findFirst('cattitle="'.$request->getPost('title').'"');

        if(!$cat){

            $mc = new Mapcategories();
            $mc->id = $guid->GUID();
            $mc->cattitle = $request->getPost('title');
            $mc->catslugs = strtolower(str_replace(' ','-',$request->getPost('title')));
            $mc->catdescription = $request->getPost('description');
            $mc->created_at = date("Y-m-d H:i:s");
            $mc->updated_at = date("Y-m-d H:i:s");

            if($mc->save() == false){
                $array = [];
                foreach ($mc->getMessages() as $message) {
                    $array[] = $message;
                }

                $mc->rollback('Cannot save markers.');
                die( json_encode(array('error' => $array)));
            }else{
                die(json_encode(array('success'=>'Success', 'category' => array('id' => $mc->id, 'title' => $mc->cattitle ))));
            }
        }else{
            die( json_encode(array('error' => 'Title already taken.')));
        }
    }

    public function getmapcategoriesAction() {
      $mc = Mapcategories::find();
      if($mc){
        echo json_encode($mc->toArray());
      }
    }

    public function titlevalidationAction($id, $title) {
      $exist = Maps::findFirst("id!='$id' AND title='$title'");
      if($exist){
        $data = array('error' => 'Title is already taken.', 'exists' => true );
      }else {
        $data = array('success' => 'success', 'exists' => false );
      }
      echo json_encode($data);
    }

    public function loadmapgalleryAction($id) {
      $mg = Mapgallery::find("map_id='$id'");
      $data = [];
      foreach ($mg->toArray() as $key => $value) {
        if($value['coverType'] == 'image'){
          $data['image'][] = array('filename' => $value['cover'], 'id' => $value['id']);
        }else {
          $data['video'][] = array('video' => $value['cover'], 'videoid' => $value['id']);
        }
      }

      echo json_encode($data);
    }

    public function saveimageAction($id, $type) {
      if($type == 'image'){
        $cover = $_POST['imgfilename'];
      }else if($type == 'video') {
        $cover = $_POST['newsvid'];
      }
      $mg = new Mapgallery();
      $guid = new \Utilities\Guid\Guid();
      $mg->assign(array(
          'id' => $guid->GUID(),
          'cover' => $cover,
          'coverType' => $type,
          'map_id' => $id
          ));

      if (!$mg->save()) {
          $data['error'] = "Something went wrong saving the data, please try again.";
      } else {
          $data['success'] = "Success";
      }

      echo json_encode($data);
    }
    public function deletemapmediaAction($id, $type)
    {
      if($type == 'image'){
        $conditions = 'cover="' . $_POST['imgfilename'] . '" AND map_id="' . $id . '"';
      }else if($type == 'video') {
        $conditions = 'id="' . $_POST['videoid'] . '" AND map_id="' . $id . '"';
      }

      $mg = Mapgallery::findFirst(array($conditions));
      if ($mg) {
        if ($mg->delete()) {
            $data = array('success' => 'Image Deleted');
        }
        else
        {
            $data = array('error' => 'Image Not Deleted');
        }
      }
      echo json_encode($data);
    }

    public function updatemapAction($id) {
      $request = new \Phalcon\Http\Request();
      if($request->isPost()){
        try{
          $transactionManager = new TransactionManager();

          $transaction = $transactionManager->get();

          $map = Maps::findFirst("id='" . $id ."'");
          $map->setTransaction($transaction);

          $title = $request->getPost('title');
          $description = str_replace('<br />', '', $request->getPost('description'));

          $tag = $request->getPost('tags');

          $conditions = 'mapid="' . $map->id . '"';
          $dlttags = Maptags::find(array($conditions));
          if ($dlttags) {
              foreach($dlttags as $dlttags){
                  if ($dlttags->delete()) {
                      $data = array('success' => 'Categories Deleted');
                  }
              }
          }

          foreach($tag as $tg){
              $find = Tags::findFirst('tag="'.strtolower($tg).'"');
              $id = $find->id;
              if(!$find){
                  $tags = new Tags();
                  $tags->setTransaction($transaction);
                  $tags->tag = strtolower($tg);
                  $tags->tagslugs = strtolower(str_replace(' ','-',$tg));
                  $tags->created_at = date("Y-m-d H:i:s");
                  $tags->updated_at = date("Y-m-d H:i:s");

                  if ($tags->save() == false) {
                      $array = [];
                      foreach ($tags->getMessages() as $message) {
                          $array[] = $message;
                      }
                  }else{
                      $id = $tags->id;
                  }
              }

              $mt = Maptags::findFirst("mapid='$map->id' AND tagid='$id'");
              if(!$mt){
                $maptags = new Maptags();
                $maptags->setTransaction($transaction);
                $maptags->mapid = $map->id;
                $maptags->tagid = $id;
                $maptags->created_at = date("Y-m-d H:i:s");
                $maptags->updated_at = date("Y-m-d H:i:s");

                if ($maptags->save() == false) {
                  $array = [];
                  foreach ($maptags->getMessages() as $message) {
                      $array[] = $message;
                  }
                }
              }
          }

          //Saving Categories
          $cat = $request->getPost('categories');

          $conditions = 'mapid="' . $map->id . '"';
          $dltcat = Mapcats::find(array($conditions));
          if ($dltcat) {
              foreach($dltcat as $dltcat){
                  if ($dltcat->delete()) {
                      $data = array('success' => 'Categories Deleted');
                  }
              }
          }

          foreach($cat as $c => $t){
            $mp = Mapcats::findFirst("mapid='$map->id' AND catid='" . $cat[$c] . "'");
            if(!$mp){
              $category = new Mapcats();
              $category->setTransaction($transaction);
              $category->mapid = $map->id;
              $category->catid = $cat[$c];
              $category->created_at = date("Y-m-d H:i:s");
              $category->updated_at = date("Y-m-d H:i:s");
              if ($category->save() == false) {
                  $array = [];
                  foreach ($maps->getMessages() as $message) {
                      $array[] = $message;
                  }
              }
            }
          }

          if($map){
              $map->title = $title;
              $map->description = nl2br($description);
              $map->mapslugs = $request->getPost("mapslugs");
              $map->coverType = $request->getPost("coverType");
              $map->cover = $request->getPost("cover");
              $map->featured = ($request->getPost("featured") == "true" ? 1 : 0);
              $map->status = ($request->getPost("status") == "true" ? 1 : 0);
              $map->updated_at = date("Y-m-d H:i:is");
              if($map->save()){
                  $data = array('success' => 'Map has been successfully edited.' );
              }else {
                  $array = [];
                  foreach ($maps->getMessages() as $message) {
                    $array[] = $message;
                  }
              }
          }else {
              $array = [];
              foreach ($maps->getMessages() as $message) {
                $array[] = $message;
              }
          }
          $transaction->commit();
          echo json_encode($data);
        }catch(\Phalcon\Mvc\Model\Transaction\Failed $e) {
          die( json_encode(array('401' => $e->getMessage())) );
        }
      }
    }

    public function bgetpinimagesAction($id) {
      $app = new CB();
      $pin = Markers::findFirst("id='$id'");
      if($pin) {
        $pinimages = Markerpics::find("marker_id='$id'");
        $agent = $app->dbSelect("SELECT first_name, last_name, profile_pic_name FROM agents WHERE id='" . $pin->agent . "'");
        $commentcount = $app->dbSelect("SELECT COUNT(*) FROM commentreply WHERE marker_id='$id'")[0]["COUNT(*)"];
        $comments = $app->dbSelect("SELECT commentreply.*, agents.first_name, agents.last_name, agents.profile_pic_name FROM commentreply LEFT JOIN agents ON commentreply.agent=agents.id WHERE marker_id='$pin->id' ORDER BY commentreply.updated_at DESC");
        foreach ($comments as $key => $value) {
            $comments[$key]['showedit'] = false;
        }
        $images = Markerpics::find("marker_id='$id'");
        $agent = Agents::findFirst("id='" . $marker['agent'] . "'");
        $map = Maps::findFirst("id='" . $marker['map_id'] . "'");

        $data = array('pinimages' => $pinimages->toArray(), 'agent' => $agent[0], 'commentcount' => $commentcount,  'comments' => $comments, 'images' => $images->toArray(), 'agent' => $agent, JSON_NUMERIC_CHECK);
      }else {
        $data = array('error' => 'An error occurred please try again later.');
      }
      echo json_encode($data);
    }

    public function getTotalAction(){

        $app = new CB();

        $sql = "SELECT COUNT(DISTINCT maps.id) as mapscount
          FROM
          maps WHERE status=1";
        $maps = $app->dbSelect($sql);

        $sql2 = "SELECT COUNT(DISTINCT markers.id) as markercount
          FROM
          markers WHERE status=1";
        $markers = $app->dbSelect($sql2);

        $sql3 = "SELECT COUNT(DISTINCT agents.id) as agentcount
          FROM
          agents WHERE status=1";
        $agents = $app->dbSelect($sql3);


        echo json_encode(array('maps'=>$maps, 'markers'=>$markers, 'agents' => $agents));

    }

    public function getAllPins(){
        $app = new CB();
        $sql = 'SELECT
            IF(maps.hide_agent=1,agents.first_name,"Anonymous") as first_name ,
            IF(maps.hide_agent=1,agents.last_name,"") as last_name,
            agents.email,
            agentagent,
            agents.username,
            agentid,
            agents.profile_pic_name,
            maps.title,
            maps.description,
            maps.created_at,
            maps.updated_at,
            maps.id as mapid,
            maps.featured,
            maps.greatest,
            maps.views,
            maps.cover,
            maps.coverType,
            maps.status,
            maps.mapslugs,
            maps.hide_agent,
            markers.lat,
            markers.long,
            mark
            markers.id,ers.pin,
            markers.status as markerstatus
              FROM

              maps INNER JOIN agents ON maps.agent = agents.id INNER JOIN markers ON maps.id = markers.map_id WHERE maps.status=1
              ';

        $allpins = $app->dbSelect($sql);

        die(json_encode(array(
         'all'=> $allpins
        )));

    }
}
