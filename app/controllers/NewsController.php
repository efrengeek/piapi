<?php

namespace Controllers;

use \Controllers\ControllerBase as CB;
use \Models\Author as Author;
use \Models\Authorimage as Authorimage;
use \Models\News as News;
use \Models\Newscategory as Newscategory;
use \Models\Newscat as Newscat;
use \Models\Newstags as Newstags;
use \Models\Newstagslist as Newstagslist;
use \Models\Newsimage as Newsimage;
use \Models\Newsvideo as Newsvideo;
use \Models\Mapnews as Mapnews;


class NewsController extends \Phalcon\Mvc\Controller {

	public function authorlistimagesAction() {
        $getimages = Authorimage::find(array("order" => "id DESC"));
        echo json_encode($getimages->toArray());
    }

    public function saveauthorimageAction() {
        $filename = $_POST['imgfilename'];
        $picture = new Authorimage();
        $picture->assign(array(
            'filename' => $filename
            ));

        if (!$picture->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";
        } else {
            $data['success'] = "Success";
        }

    }

    public function deleteauthorimgAction()
    {
        $conditions = 'filename="' . $_POST['imgfilename'] . '"';
        $newsimg = Authorimage::findFirst(array($conditions));
        if ($newsimg) {
            if ($newsimg->delete()) {
                $data = array('success' => 'Image Deleted');
            }
            else
            {
                $data = array('error' => 'Image Not Deleted');
            }
        }
        echo json_encode($data);
    }

    public function createAuthorAction() {

        $request = new \Phalcon\Http\Request();

        if($request->isPost()){


            $name = $request->getPost('name');
            $about = $request->getPost('about');
            $location = $request->getPost('location');
            $occupation = $request->getPost('occupation');
            $photo = $request->getPost('photo');
            $since = $request->getPost('since');


            $guid = new \Utilities\Guid\Guid();
            $id = $guid->GUID();

            $author = new Author();
            $author->assign(array(
                'authorid' => $id,
                'name' => $name,
                'location' => $location,
                'occupation' => $occupation,
                'about' => $about,
                'image' => $photo,
                'date_created' => $since,
                'date_updated' => date('Y-m-d')
                ));

            if (!$author->save()) {
                $errors = array();
                foreach ($author->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
            } else {
                $data['success'] = "Success";

            }

        }

        echo json_encode($data);
    }

    public function manageAuthorAction($num, $page, $keyword) {

        $app = new CB();

        // offsetting
        $offsetfinal = ($page * 10) - 10;
        $sql = 'SELECT * FROM author';
        $sqlCount = 'SELECT COUNT(*) FROM author';

        if ($keyword != 'null' && $keyword != 'undefined') {
            $keyword = addslashes($keyword);
            $sqlconcat = " WHERE name LIKE '%" . $keyword . "%' OR location LIKE '%" . $keyword . "%' OR occupation LIKE '%" . $keyword . "%' OR date_created LIKE '%" . $keyword . "%'";;
            $sql .= $sqlconcat;
            $sqlCount .= $sqlconcat;
        }

        if($offsetfinal < 0){
            $offsetfinal = 0;
        }
        $sql .= " ORDER BY name ASC ";
        $sql .= " LIMIT " . $offsetfinal . ",10";
        // getting the query
        $searchresult = $app->dbSelect($sql);

        $totalreportdirty = $app->dbSelect($sqlCount);

        echo json_encode(array('data' => $searchresult, 'index' =>$page, 'total_items' => $totalreportdirty[0]["COUNT(*)"]));
    }

    public function loadauthornewsAction($authorid){
        $app = new CB();
        $sql = "SELECT * FROM news LEFT JOIN author on news.author = author.authorid WHERE news.author='$authorid'";
        $data = $app->dbSelect($sql);

        $sql = "SELECT * FROM author WHERE authorid != '$authorid'";
        $authorarray = $app->dbSelect($sql);

        echo json_encode(array("dataconflict" => $data, "newsauthors" => $authorarray));
    }

    public function authordeleteAction($authorid) {

        $conditions = 'authorid="' . $authorid . '"';
        $author = Author::findFirst(array($conditions));
        $data = array('error' => 'Not Found');
        if ($author) {
            if ($author->delete()) {
                $data = array('success' => 'Author Deleted');
            }
        }
        echo json_encode($data);
    }

    public function authoreditoAction($authorid) {
        $data = array();

        $author = Author::findFirst('authorid="' . $authorid . '"');
        if ($author) {
            $data = array(
                'authorid' => $author->authorid,
                'name' => $author->name,
                'location' => $author->location,
                'occupation' => $author->occupation,
                'about' => $author->about,
                'photo' => $author->image,
                'date_created' => $author->date_created

                );
        }
        echo json_encode($data);
    }

    public function authorupdateAction() {

       $request = new \Phalcon\Http\Request();

        if($request->isPost()){

            $authorid = $request->getPost('authorid');
            $name = $request->getPost('name');
            $location = $request->getPost('location');
            $occupation = $request->getPost('occupation');
            $about = $request->getPost('about');
            $photo = $request->getPost('photo');

            $author = Author::findFirst('authorid="' . $authorid . '"');
            $author->name = $name;
            $author->location = $location;
            $author->occupation = $occupation;
            $author->about = $about;
            $author->image = $photo;

            if (!$author->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
            } else {
                $data['success'] = "Success";

            }
            echo json_encode($data);

        }
    }

    public function managecategoryAction($num, $page, $keyword) {

        if ($keyword == 'null' || $keyword == 'undefined') {
            $Pages = Newscategory::find();
        } else {
            $keyword = addslashes($keyword);
            $conditions = "categoryname LIKE '%" . $keyword . "%'";
            $Pages = Newscategory::find(array($conditions));
        }

        $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
        $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $Pages,
                "limit" => 10,
                "page" => $currentPage
                )
            );

        // Get the paginated results
        $page = $paginator->getPaginate();

        $data = array();
        foreach ($page->items as $m) {
            $data[] = array(
                'categoryid' => $m->categoryid,
                'categoryname' => $m->categoryname,
                );
        }
        $p = array();
        for ($x = 1; $x <= $page->total_pages; $x++) {
            $p[] = array('num' => $x, 'link' => 'page');
        }
        echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));

    }

    public function createcategoryAction()
    {

        $findNewsCategory = Newscategory::findFirst('categoryname = "'.$_POST['catnames'].'"');
        if($findNewsCategory){
            die(json_encode(array('error' => array('existCategory' => 'Category is already existing'))));
        }

        $data = array();
		$guid = new \Utilities\Guid\Guid();
        $id = $guid->GUID();

       $catnames = new Newscategory();
       $catnames->assign(array(
	    'categoryid' => $id,
        'categoryname' => $_POST['catnames'],
        'categoryslugs' => $_POST['slugs'],
        ));
        if (!$catnames->save()){
        $errors = array();
                foreach ($catnames->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
        }
        else{
         $data['success'] = "Success";
		 $data['categoryid'] = $id;
        }
        echo json_encode($data);
    }

    public function loadconflictsAction($id) {
        $app = new CB();
        $sql = "SELECT news.title, news.newsid, author.name FROM newscat INNER JOIN news ON newscat.newsid = news.newsid INNER JOIN author ON news.author = author.authorid WHERE newscat.catid='$id'";
        $data = $app->dbSelect($sql);

        $sql = "SELECT categoryid, categoryname from newscategory WHERE categoryid != '$id' ";
        $categoryarray = $app->dbSelect($sql);


        echo json_encode(array("dataconflict" => $data, "newscategory" => $categoryarray));
    }

    public function updateconflictAction() {
        $request = new \Phalcon\Http\Request();

        if($request->isPost()){
            $id = $request->getPost('newsid');
            $catid = $request->getPost('catid');
            $cat = Newscat::findFirst("newsid='$id' AND catid='$catid'");
            $newscat = Newscat::findFirst('newsid="'.$id.'"');
            if(!$cat){
                $newscat->catid = $catid;
                $newscat->save();
            }else {
                $newscat->delete();
            }
        }
    }

    public function categorydeleteAction($id) {
        $conditions = "categoryid='" . $id . "'";
        $news = Newscategory::findFirst(array($conditions));
        $data = array('error' => 'Not Found');
        if ($news) {
            if ($news->delete()) {
                $data = array('success' => 'Category Deleted');
            }
        }
        echo json_encode($data);
    }

    public function updatecategoryAction($catname,$id)
    {

        $find = Newscategory::findFirst('categoryid!=' . $id . ' AND categoryname="'.$catname.'" ');
        if($find){
            $data['error'] = "Your data already exist.";
            echo json_encode($data);
        }else{
            $data = array();
            $news = Newscategory::findFirst('categoryid=' . $id . '');
            $news->categoryname = $catname;
            $news->categoryslugs = rawurlencode(str_replace('%20', ' ', $catname));
            if (!$news->save())
            {
                $data['error'] = "Something went wrong saving the data, please try again.";
            }
            else
            {
                $data['success'] = "Success";
            }
            echo json_encode($data);
        }
    }

    public function managetagsAction($num, $page, $keyword) {

        if ($keyword == 'null' || $keyword == 'undefined') {
            $tag = Newstags::find();
        } else {
            $conditions = "tags LIKE '%" . $keyword . "%'";
            $tag = Newstags::find(array($conditions));
        }

        $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
        $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $tag,
                "limit" => 10,
                "page" => $currentPage
                )
            );

        // Get the paginated results
        $page = $paginator->getPaginate();

        $data = array();
        foreach ($page->items as $m) {
            $data[] = array(
                'id' => $m->id,
                'tags' => $m->tags,
                );
        }
        $p = array();
        for ($x = 1; $x <= $page->total_pages; $x++) {
            $p[] = array('num' => $x, 'link' => 'page');
        }
        echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));

    }

     public function createtagsAction()
    {

        $findNewstags = Newstags::findFirst('tags="'.$_POST['tags'].'"');
        if($findNewstags){
            die(json_encode(array('error' => array('existTags' => 'Tag is already existing'))));
        }

       $data = array();

       $tagsnames = new Newstags();
       $tagsnames->assign(array(
        'tags' => $_POST['tags'],
        'slugs' => rawurlencode(str_replace("%20", " ", $_POST['tags'])),
        ));
        if (!$tagsnames->save()){
        $errors = array();
                foreach ($tagsnames->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
        }
        else{
         $data['success'] = "Success";
         $data['data'] = $tagsnames;
        }
        echo json_encode($data);
    }

    public function tagconflictAction($id) {
        $app = new CB();
        $sql = "SELECT news.newsid, news.title, newstagslist.tags, author.name FROM newstagslist INNER JOIN news ON newstagslist.newsid = news.newsid INNER JOIN author ON news.author = author.authorid WHERE newstagslist.tags=$id";
        $data = $app->dbSelect($sql);

        $sql = "SELECT id, tags from newstags WHERE id != $id ";
        $tagsarray = $app->dbSelect($sql);


        echo json_encode(array("dataconflict" => $data, "newstags" => $tagsarray));
    }

    public function updateTagConflictAction(){
        $request = new \Phalcon\Http\Request();

        if($request->isPost()){
            $newsid = $request->getPost('newsid');
            $tags = $request->getPost('id');

            $tag = Newstagslist::findFirst("newsid='$newsid' AND tags='$tags'");
            $newstags = Newstagslist::findFirst('newsid="'.$newsid.'"');
            if(!$tag){
                $newstags->tags = $tags;
                $newstags->save();
            }else {
                $newstags->delete();
            }
        }
    }

    public function tagsdeleteAction($id) {
        $conditions = "id=" . $id;
        $news = Newstags::findFirst(array($conditions));
        $data = array('error' => 'Not Found');
        if ($news) {
            if ($news->delete()) {
                $data = array('success' => 'Category Deleted');
            }
        }
        echo json_encode($data);
    }

    public function updatetagsAction($tagname,$id)
    {

        $find = Newstags::findFirst('id!=' . $id . ' AND tags = "'.$tagname.'"');
        if($find){
            $data['error'] = "Your data already exist.";
            echo json_encode($data);
        }else {
            $data = array();
            $news = Newstags::findFirst('id=' . $id . ' ');
            $news->tags = $tagname;
            $news->slugs = rawurlencode(str_replace('%20', ' ', $tagname));

            if (!$news->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
            } else {
                $data['success'] = "Success";
            }
            echo json_encode($data);
        }
    }

    public function listimageAction() {

        $getimages = Newsimage::find(array("order" => "id DESC"));
        foreach ($getimages as $getimages)
        {
            $data[] = array(
                'id'=>$getimages->id,
                'filename'=>$getimages->filename
                );
        }
        echo json_encode($data);

    }

    public function listvideoAction() {

        $getvid = Newsvideo::find(array("order" => "num DESC"));
        foreach ($getvid as $getvid)
        {
            $data[] = array(
                'videoid'=>$getvid->videoid,
                'video'=>$getvid->video
                );
        }
        echo json_encode($data);

    }

    public function saveimageAction() {
        $filename = $_POST['imgfilename'];
        $picture = new Newsimage();
        $picture->assign(array(
            'filename' => $filename
            ));

        if (!$picture->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";
        } else {
            $data['success'] = "Success";
        }
    }

    public function savevideoAction() {
        $video = $_POST['newsvid'];

        $guid = new \Utilities\Guid\Guid();
        $videoid = $guid->GUID();

        $vid = new Newsvideo();
        $vid->assign(array(
            'videoid' => $videoid,
            'video' => $video
            ));

        if (!$vid->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";
        } else {
            $data['success'] = "Success";
        }
        echo json_encode($data);
    }

    public function deletenewsimgAction()
    {
        $conditions = 'filename="' . $_POST['imgfilename'] . '"';
        $newsimg = Newsimage::findFirst(array($conditions));
        if ($newsimg) {
            if ($newsimg->delete()) {
                $data = array('success' => 'Image Deleted');
            }
            else
            {
                $data = array('error' => 'Image Not Deleted');
            }
        }
        echo json_encode($data);
    }

    public function deletevideoAction()
    {
        $conditions = 'videoid="' . $_POST['videoid'] . '"';
        $newsvid = Newsvideo::findFirst(array($conditions));
        if ($newsvid) {
            if ($newsvid->delete()) {
                $data = array('success' => 'Image Deleted');
            }
            else
            {
                $data = array('error' => 'Image Not Deleted');
            }
        }
        echo json_encode($data);
    }

    public function listAuthorAction()
    {

        $getAuthor= Author::find(array("order" => "authorid ASC"));
        foreach ($getAuthor as $getAuthor) {
            $data[] = array(
                'authorid'=>$getAuthor->authorid,
                'name'=>$getAuthor->name,
                'about'=>$getAuthor->about,
                'image'=>$getAuthor->image
                );
        }
        echo json_encode($data);

    }

    public function listcategoryAction()
    {

        $getcategory= Newscategory::find(array("order" => "categoryid ASC"));
        foreach ($getcategory as $getcategory) {
            $data[] = array(
                'categoryid'=>$getcategory->categoryid,
                'categoryname'=>$getcategory->categoryname
                );
        }
        echo json_encode($data);

    }

    public function listtagsAction()
    {

        $getcategory= Newstags::find(array("order" => "id ASC"));
        foreach ($getcategory as $getcategory) {
            $data[] = array(
                'id'=>$getcategory->id,
                'tags'=>$getcategory->tags,
                'slugs'=>$getcategory->slugs
                );
        }
        echo json_encode($data);

    }

    public function createNewsAction() {
        $request = new \Phalcon\Http\Request();
        if($request->isPost()){
            $title = $request->getPost('title');
            $slugs = $request->getPost('slugs');
            $author = $request->getPost('author');
            $date = $request->getPost('date');
            $summary = $request->getPost('summary');
            $body = $request->getPost('body');
            $videothumb = '';
            $imagethumb = '';
            if($request->getPost('featuredthumbtype')=='video'){
                $videothumb = $request->getPost('featuredthumb');
            }elseif($request->getPost('featuredthumbtype')=='image'){
                $imagethumb = $request->getPost('featuredthumb');
            }
            $status = $request->getPost('status');
            $category = $request->getPost('category');
            $tags = $request->getPost('tag');
            $metatitle = $request->getPost('metatitle');
            $metakeyword = $request->getPost('metakeyword');
            $metadesc = $request->getPost('metadesc');
            $featurednews = $request->getPost('featurednews') == true ? 1 : 0;

            $findNews = News::findFirst('title="'.$title.'"');
            if($findNews){
                die(json_encode(array('error' => array('existTitle' => 'Title is already existing'))));
            }



            $guid = new \Utilities\Guid\Guid();
            $newsid = $guid->GUID();

            $page = new News();
            $page->assign(array(
                'newsid' => $newsid,
                'title' => $title,
                'newsslugs' => $slugs,
                'summary' => $summary,
                'author' => $author,
                'body' => $body,
                'imagethumb' => $imagethumb,
                'videothumb' => $videothumb,
                'date' => $date,
                'status' => $status,
                'views' => 1,
                'type' => 'main',
                'featurednews' => $featurednews,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' =>date("Y-m-d H:i:s"),
                'metatitle' => $metatitle,
                'metakeyword' => $metakeyword,
                'metadesc' => $metadesc
                ));

            if (!$page->save()) {
                $errors = array();
                foreach ($page->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
            } else {

                foreach($category as $cat){
                    $catnews = new Newscat();
                    $catnews->assign(array(
                        'id' => $guid->GUID(),
                        'newsid' => $newsid,
                        'catid' => $cat
                    ));
                    if (!$catnews->save())
                    {
                        $errors = array();
                        foreach ($page->getMessages() as $message) {
                            $errors[] = $message->getMessage();
                        }
                        echo json_encode(array('error' => $errors));
                    }
                    else
                    {
                        $data['success'] = "Success cat";
                    }
                }

                $newstags = $tags;
                foreach($newstags as $nt){
                    $gettags = Newstags::findFirst("tags='$nt'");
                    if(!$gettags){
                        var_dump($nt);
                        $newstags = new Newstags();
                        $newstags->assign(array(
                            'newsid' => $newsid,
                            'tags' => $nt,
                            'slugs' => str_replace("-", " ", $nt)
                        ));

                        if (!$newstags->save())
                        {
                            $data['error'] = "Something went wrong saving the newstags, please try again.";
                        }
                        else
                        {
                            $tagsofnews = new Newstagslist();
                            $tagsofnews->assign(array(
                                'newsid' => $newsid,
                                'tags' => $newstags->id
                            ));
                            if (!$tagsofnews->save())
                            {
                                $data['error'] = "Something went wrong saving the tags, please try again.";
                            }
                            else
                            {
                                $data['success'] = "Success Tags";
                            }
                        }

                    }else{
                        $data['success'] = "Success";
                        $tagsofnews = new Newstagslist();
                        $tagsofnews->assign(array(
                            'newsid' => $newsid,
                            'tags' => $gettags->id
                        ));
                        if (!$tagsofnews->save())
                        {
                            $data['error'] = "Something went wrong saving the newstags, please try again.";
                        }
                        else
                        {
                            $data['success'] = "Success newstags";
                        }
                    }
                }
            }
        }
        echo json_encode($data);
    }

    public function manageNewsAction($num, $page, $keyword, $sort) {
        $app = new CB();

        // offsetting
        $offsetfinal = ($page * 10) - 10;
        $sql = 'SELECT * FROM news INNER JOIN author ON author.authorid = news.author';
        $sqlCount = 'SELECT COUNT(*) FROM news INNER JOIN author ON author.authorid = news.author';

        if ($keyword != 'null' && $keyword != 'undefined') {
            $keyword = addslashes($keyword);
            $sqlconcat = " WHERE news.title LIKE '%" . $keyword . "%' OR author.name LIKE '%".$keyword."%' ";
            $sql .= $sqlconcat;
            $sqlCount .= $sqlconcat;
        }

        if($offsetfinal < 0){
            $offsetfinal = 0;
        }
        if($sort == "date" || $sort == "featurednews" || $sort == "updated_at"){
            $sql .= " ORDER BY ".$sort." DESC ";
        }else {
            $sql .= " ORDER BY ".$sort." ASC ";
        }

        $sql .= " LIMIT " . $offsetfinal . ",10";
        // getting the query
        $searchresult = $app->dbSelect($sql);

        $totalreportdirty = $app->dbSelect($sqlCount);

        echo json_encode(array('data' => $searchresult, 'index' =>$page, 'total_items' => $totalreportdirty[0]["COUNT(*)"]));
    }

    public function newsdeleteAction($newsid) {
        $conditions = 'newsid="' . $newsid . '"';
        $news = News::findFirst(array($conditions));
        $data = array('error' => 'Not Found');
        if ($news) {
            if ($news->delete()) {
                $data = array('success' => 'News Deleted');
            }
        }
        echo json_encode($data);
    }

    public function newsUpdatestatusAction($status,$newsid) {

        $data = array();
        $news = News::findFirst('newsid="' . $newsid . '"');
        $news->status = $status;
        if($status == 1){
            $news->date = date("Y-m-d");
        }
        if (!$news->save()) {
            $news['error'] = "Something went wrong saving page status, please try again.";
        } else {
            $data['success'] = "Success";
        }

        echo json_encode($data);
    }

    public function newseditoAction($newsid) {
        $app = new CB();
        $data = array();

        $news = News::findFirst('newsid="' . $newsid . '"');
        if ($news) {
            $sql = "SELECT id, newstags.tags FROM newstags INNER JOIN newstagslist ON newstags.id = newstagslist.tags WHERE newstagslist.newsid = '" . $newsid . "'";
            $searchresult1 = $app->dbSelect($sql);

            $sql = "SELECT categoryid, categoryname FROM newscat INNER JOIN newscategory ON newscat.catid = newscategory.categoryid WHERE newscat.newsid = '" . $newsid . "'";
            $searchresult2 = $app->dbSelect($sql);


            $data = array(
                'newsid' => $news->newsid,
                'title' => $news->title,
                'slugs' => $news->newsslugs,
                'author' => $news->author,
                'summary' => $news->summary,
                'body' => $news->body,
                'imagethumb' => $news->imagethumb,
                'videothumb' => $news->videothumb,
                'newslocation' => $news->newslocation,
                'category' => $searchresult2,
                'status' => $news->status,
                'featurednews' => $news->featurednews,
                'date' => $news->date,
                'created_at' => $news->created_at,
                'tag' => $searchresult1,
                'metatitle' => $news->metatitle,
                'metakeyword' => $news->metakeyword,
                'metadesc' => $news->metadesc
                );
        }
        echo json_encode($data);
    }

    public function updateNewsAction() {

        $request = new \Phalcon\Http\Request();

        if($request->isPost()){

            $newsid = $request->getPost('newsid');
            $title = $request->getPost('title');
            $slugs = $request->getPost('slugs');
            $author = $request->getPost('author');
            $date = $request->getPost('date');
            $summary = $request->getPost('summary');
            $body = $request->getPost('body');
            $videothumb = '';
            $imagethumb = '';
            if($request->getPost('featuredthumbtype')=='video'){
                $videothumb = $request->getPost('featuredthumb');
            }elseif($request->getPost('featuredthumbtype')=='image'){
                $imagethumb = $request->getPost('featuredthumb');
            }
            $newslocation = $request->getPost('newslocation');
            $category = $request->getPost('category');
            // $featurednews = $request->getPost('featurednews');
            // $datecreated = $request->getPost('datecreated');
            $status = $request->getPost('status');
            $tags = $request->getPost('tag');
            $metatitle = $request->getPost('metatitle');
            $metakeyword = $request->getPost('metakeyword');
            $metadesc = $request->getPost('metadesc');

            if($imagethumb != ""){
                $videothumb = "";
            }else if($videothumb != ""){
                $imagethumb = "";
            }

            $findNews = News::findFirst('title="'.$title.'" AND newsid!="'.$newsid.'"');
            if($findNews){
                die(json_encode(array('error' => array('existTitle' => 'Title is already existing'))));
            }

            $news = News::findFirst('newsid="' . $newsid . '"');
            $news->title = $title;
            $news->newsslugs = $slugs;
            $news->author = $author;
            $news->summary = $summary;
            $news->body = $body;
            $news->imagethumb = $imagethumb;
            $news->videothumb = $videothumb;
            $news->newslocation = $newslocation;
            $news->date = $date;
            $news->updated_at = date('Y-m-d');
            $news->metatitle = $metatitle;
            $news->metakeyword = $metakeyword;
            $news->metadesc = $metadesc;
            $news->status = $status;

            if (!$news->save()) {
                $errors = array();
                foreach ($news->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
            } else {

                $conditions = 'newsid="' . $newsid . '"';
                $deletenewstags = Newstagslist::find(array($conditions));
                $data = array('error' => 'Not Found');
                if ($deletenewstags) {
                    foreach($deletenewstags as $deletenewstags){
                        if ($deletenewstags->delete()) {
                            $data = array('success' => 'tags Deleted');
                        }
                    }
                }

                $conditions = 'newsid="' . $newsid . '"';
                $deletenewscats = Newscat::find(array($conditions));
                $data = array('error' => 'Not Found');
                if ($deletenewscats) {
                    foreach($deletenewscats as $deletenewscats){
                        if ($deletenewscats->delete()) {
                            $data = array('success' => 'Categories Deleted');
                        }
                    }
                }
                $guid = new \Utilities\Guid\Guid();
                foreach($category as $cat){
                    $catnews = new Newscat();
                    $catnews->assign(array(
                        'id' => $guid->GUID(),
                        'newsid' => $newsid,
                        'catid' => $cat
                    ));
                    if (!$catnews->save())
                    {
                        $errors = array();
                        foreach ($page->getMessages() as $message) {
                            $errors[] = $message->getMessage();
                        }
                        echo json_encode(array('error' => $errors));
                    }
                    else
                    {
                        $data['success'] = "Success cat";
                    }
                }

                $newstags = $tags;
                foreach($newstags as $nt){
                    $gettags = Newstags::findFirst("tags='$nt'");
                    if(!$gettags){
                        var_dump($nt);
                        $newstags = new Newstags();
                        $newstags->assign(array(
                            'newsid' => $newsid,
                            'tags' => $nt,
                            'slugs' => str_replace("-", " ", $nt)
                        ));

                        if (!$newstags->save())
                        {
                            $data['error'] = "Something went wrong saving the newstags, please try again.";
                        }
                        else
                        {
                            $tagsofnews = new Newstagslist();
                            $tagsofnews->assign(array(
                                'newsid' => $newsid,
                                'tags' => $newstags->id
                            ));
                            if (!$tagsofnews->save())
                            {
                                $data['error'] = "Something went wrong saving the tags, please try again.";
                            }
                            else
                            {
                                $data['success'] = "Success Tags";
                            }
                        }

                    }else{
                        $data['success'] = "Success";
                        $tagsofnews = new Newstagslist();
                        $tagsofnews->assign(array(
                            'newsid' => $newsid,
                            'tags' => $gettags->id
                        ));
                        if (!$tagsofnews->save())
                        {
                            $data['error'] = "Something went wrong saving the newstags, please try again.";
                        }
                        else
                        {
                            $data['success'] = "Success newstags else";
                        }
                    }
                }
            }
        }
        echo json_encode($data);
    }

    public function updatenewsauthorsAction() {
        $request = new \Phalcon\Http\Request();

        if($request->isPost()){
            $newsid = $request->getPost('newsid');
            $author = $request->getPost('authorid');

            $news = News::findFirst('newsid="' . $newsid . '"');
            $news->author = $author;
            $news->save();
        }
    }

    public function newsUpdateFeaturedAction($id, $featured) {
        $news = News::findFirst("newsid='$id'");
        if($news){
            $news->featurednews = $featured;
            if($news->save()){
                $data = array('success' => 'success');
            }else {
                $data = array('error' => 'error');
            }
            echo json_encode($data);
        }
    }

    public function savemapnewsAction($id){
      $request = new \Phalcon\Http\Request();
      if($request->isPost()){
        $news = new Mapnews();
        $guid = new \Utilities\Guid\Guid();
        $news->id = $guid->GUID();
        $news->news = $request->getPost('news');
        $news->coverType = $request->getPost('coverType');
        $news->cover = $request->getPost('coverType') == "image" ? $request->getPost('image') : $request->getPost('videolink');
        $news->created_at = date('Y-m-d H:i:s');
        $news->updated_at = date('Y-m-d H:i:s');
        $news->mapid = $id;

        if($news->save()){
          $data = array('success' => 'Announcement has been published successfully');
        }else {
          $data = array('error' => 'An error occurred please try again later.');
        }
        echo json_encode($data);
      }
    }

    public function getnewsAction($limit) {
        $app = new CB();
        $sql = "SELECT * FROM news WHERE status=1 ORDER BY created_at DESC LIMIT $limit";
        $news = $app->dbSelect($sql);

        $sql = "SELECT COUNT(*) FROM news WHERE status=1 ORDER BY created_at DESC LIMIT $limit";
        $count = $app->dbSelect($sql)[0]['COUNT(*)'];

        echo json_encode(array('news' => $news, 'count' => $count));
    }

    public function validateslugAction($slug, $id) {
        if($id == 'null') {
            $condition = "newsslugs='$slug'";
        }else {
            $condition = "newsslugs='$slug' AND newsid != '$id'"; 
        }
        $news = News::findFirst($condition);
        if($news){
            echo json_encode(array('exist' => true ));
        }else {
            echo json_encode(array('exist' => false ));
        }
    }

    public function getauthorAction($id) {
        $author = Author::findFirst("authorid='$id'");
        if($author){
            echo json_encode($author);
        }
    }
}
