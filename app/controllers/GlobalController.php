<?php

namespace Controllers;

class GlobalController {

    public function modelsManager($phql) {
        $DI = \Phalcon\DI::getDefault();
        $app = $DI->get('application');
        return $result = $app->modelsManager->executeQuery($phql);
    }

    public function getConfig(){
        require_once('../config/config.php');
    }

    public function dbSelect($phql) {
        $db1 = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db1->prepare($phql);
        $stmt->execute();
        $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $searchresult;
    }

    public function sendMail($email, $subject,$content){
        $DI = \Phalcon\DI::getDefault();
        $app = $DI->get('application');
        $json = json_encode(array(
            'From' => $app->config->postmark->signature,
            'To' => $email,
            'Subject' => $subject,
            'HtmlBody' => $content
        ));

        $ch2 = curl_init();
        curl_setopt($ch2, CURLOPT_URL, $app->config->postmark->url);
        curl_setopt($ch2, CURLOPT_POST, true);
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
            'Content-Type: application/json',
            'X-Postmark-Server-Token: '.$app->config->postmark->token
        ));
        curl_setopt($ch2, CURLOPT_POSTFIELDS, $json);
        $response = json_decode(curl_exec($ch2), true);
        $http_code = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
        curl_close($ch2);
    }

    public function storeRedis($id,$token){
        $DI = \Phalcon\DI::getDefault();
        $app = $DI->get('application');
        try {
            // Cache data for 2 days
            $frontCache = new \Phalcon\Cache\Frontend\Data(array(
                "lifetime" => $app->config->redis->dataexpiration
            ));

            //Create the Cache setting redis connection options
            $cache = new \Phalcon\Cache\Backend\Redis($frontCache, array(
                'host' => $app->config->redis->host,
                'port' => $app->config->redis->port,
                'persistent' => $app->config->redis->persistent
            ));

            //Cache arbitrary data
            return $cache->save($app->config->redis->sessionkey . $id, $token);

        }
        catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function removeRedis($id){
        $DI = \Phalcon\DI::getDefault();
        $app = $DI->get('application');
        try {
            // Cache data for 2 days
            $frontCache = new \Phalcon\Cache\Frontend\Data(array(
                "lifetime" => $app->config->redis->dataexpiration
            ));

            //Create the Cache setting redis connection options
            $cache = new \Phalcon\Cache\Backend\Redis($frontCache, array(
                'host' => $app->config->redis->host,
                'port' => $app->config->redis->port,
                'persistent' => $app->config->redis->persistent
            ));

            //Cache arbitrary data
            return $cache->delete($app->config->redis->sessionkey . $id);
        }
        catch (Exception $e) {
            die($e->getMessage());
        }
    }

}
