<?php


/**
 * @SWG\Swagger(
 *     schemes={"http", "https"},
 *     host="pi.api",
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="Planet Impossible",
 *         description="Just a API Documentation for the PI",
 *         @SWG\Contact(
 *             email="efren.bautista.jr@geeksnest.com, efrenbautistajr@gmail.com"
 *         )
 *     )
 * )
 */

/**
 * @author Jete O'Keeffe
 * @version 1.0
 * @link http://docs.phalconphp.com/en/latest/reference/micro.html#defining-routes
 * @eg.

$routes[] = [
 	'method' => 'post',
	'route' => '/api/update',
	'handler' => 'myFunction'
];

 */

$routes[] = [
    'method' => 'get',
    'route' => '/testingquery',
    'handler' => ['Controllers\ControllerBase', 'testingQuery'],
    'authentication' => FALSE
];

$routes[] = [
	'method' => 'post',
	'route' => '/ping',
	'handler' => ['Controllers\ExampleController', 'pingAction'],
    'authentication' => FALSE
];

$routes[] = [
	'method' => 'post',
	'route' => '/test/{id}',
	'handler' => ['Controllers\ExampleController', 'testAction']
];

$routes[] = [
    'method' => 'get',
    'route' => '/authenticate/get',
    'handler' => ['Controllers\AuthenticateController', 'getAccessTokenAction'],
    'authentication' => TRUE,
    'level' => 'all'
];

$routes[] = [
    'method' => 'get',
    'route' => '/user/logout',
    'handler' => ['Controllers\UserController', 'logoutAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/agent/logout',
    'handler' => ['Controllers\AgentController', 'logoutAction'],
    'authentication' => TRUE,
    'level' => 'agent'
];

$routes[] = [
	'method' => 'post',
	'route' => '/skip/{name}',
	'handler' => ['Controllers\ExampleController', 'skipAction'],
    'authentication' => FALSE
];
/* AGENT */
/* Validation for username exist */
$routes[] = [
    'method' => 'get',
    'route' => '/agent/usernameexist/{name}',
    'handler' => ['Controllers\AgentController', 'userExistAction'],
    'authentication' => FALSE
];
/* Validation for email exist */
$routes[] = [
    'method' => 'get',
    'route' => '/agent/emailexist/{name}',
    'handler' => ['Controllers\AgentController', 'emailExistAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/agent/register',
    'handler' => ['Controllers\AgentController', 'registerUserAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/agent/getpinAgent/{id}',
    'handler' => ['Controllers\AgentController', 'getpinAgentAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/user/activation',
    'handler' => ['Controllers\AgentController', 'activationAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/agent/login',
    'handler' => ['Controllers\AgentController', 'loginAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/agent/get/{username}',
    'handler' => ['Controllers\AgentController', 'getInfo'],
    'authentication' => TRUE,
    'level' => 'agent'
];
$routes[] = [
    'method' => 'post',
    'route' => '/agent/updateprofile',
    'handler' => ['Controllers\AgentController', 'updateprofileAction'],
    'authentication' => TRUE,
    'level' => 'agent'
];
$routes[] = [
    'method' => 'post',
    'route' => '/agent/updateprofilepic',
    'handler' => ['Controllers\AgentController', 'saveprofilepicAction'],
    'authentication' => TRUE,
    'level' => 'agent'
];
$routes[] = [
    'method' => 'post',
    'route' => '/agent/deleteprofpic/{agentid}',
    'handler' => ['Controllers\AgentController', 'deletepicAction'],
    'authentication' => TRUE,
    'level' => 'agent'
];
$routes[] = [
    'method' => 'get',
    'route' => '/agent/notification/{id}',
    'handler' => ['Controllers\AgentController', 'notificationAction'],
    'authentication' => TRUE,
    'level' => 'agent'
];
$routes[] = [
    'method' => 'post',
    'route' => '/agent/checkpassword/{id}',
    'handler' => ['Controllers\AgentController', 'checkpasswordAction'],
    'authentication' => TRUE,
    'level' => 'agent'
];
$routes[] = [
    'method' => 'post',
    'route' => '/agent/frontend/updateAcct/{id}',
    'handler' => ['Controllers\AgentController', 'updateUsernameEmailACtion'],
    'authentication' => TRUE,
    'level' => 'agent'
];
$routes[] = [
    'method' => 'post',
    'route' => '/agent/changepassword/{id}',
    'handler' => ['Controllers\AgentController', 'changepassAction'],
    'authentication' => TRUE,
    'level' => 'agent'
];
$routes[] = [
    'method' => 'get',
    'route' => "/agent/loadgallery/{id}",
    'handler' => ['Controllers\AgentController', 'loadgalleryAction'],
    'authentication' => TRUE,
    'level' => 'agent'
];
/* MAPS */
$routes[] = [
    'method' => 'post',
    'route' => '/map/marker/upload',
    'handler' => ['Controllers\MapController', 'uploadPicsAction'],
    'authentication' => TRUE,
    'level' => 'agent'
];

$routes[] = [
    'method' => 'post',
    'route' => '/map/create/info',
    'handler' => ['Controllers\MapController', 'saveMapMarkerAction'],
    'authentication' => TRUE,
    'level' => 'agent'
];

$routes[] = [
	'method' => 'get',
	'route' => '/map/titleexist/{title}',
	'handler' => ['Controllers\MapController', 'titleExistAction'],
	'authentication' => TRUE,
    'level' => 'all'
];
$routes[] = [
    'method' => 'post',
    'route' => '/map/changestatus/{id}/{stat}',
    'handler' => ['Controllers\MapController', 'changestatAction'],
    'authentication' => TRUE,
    'level' => 'all'
];
$routes[] = [
	'method' => 'get',
	'route' => '/agent/missions/list/{num}/{off}/{status}/{cat}',
	'handler' => ['Controllers\MapController', 'getMaps'],
	'authentication' => FALSE
];
$routes[] = [
	'method' => 'get',
	'route' => '/map/getpins/{title}/{agentid}',
	'handler' => ['Controllers\MapController', 'getpinsAction'],
	'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/map/hideagent/{id}/{hideagent}',
    'handler' => ['Controllers\MapController', 'maphideagentAction'],
    'authentication' => TRUE,
    'level' => 'agent'
];

$routes[] = [
    'method' => 'get',
    'route' => '/map/approvepins/{id}/{val}',
    'handler' => ['Controllers\MapController', 'approvepinsAction'],
    'authentication' => TRUE,
    'level' => 'agent'
];


$routes[] = [
    'method' => 'post',
    'route' => '/map/update/{id}',
    'handler' => ['Controllers\MapController', 'updatemapinfoAction'],
    'authentication' => TRUE,
    'level' => 'all'
];
$routes[] = [
    'method' => 'get',
    'route' => '/getmapCatTags/{id}',
    'handler' => ['Controllers\MapController', 'getmapCatTags'],
    'authentication' => TRUE,
    'level' => 'all'
];
$routes[] = [
    'method' => 'get',
    'route' => '/getpopulartags',
    'handler' => ['Controllers\MapController', 'getpopulartagsAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/searchMap/{off}/{keyword}/{category}/{tag}',
    'handler' => ['Controllers\MapController', 'searchmapAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/getagentMaps/{id}',
    'handler' => ['Controllers\MapController', 'getagentmapsAction'],
    'authentication' => TRUE,
    'level' => 'agent'
];
$routes[] = [
	'method' => 'post',
	'route' => '/save/markerimage',
	'handler' => ['Controllers\MapController', 'savepinimageAction'],
	'authentication' => TRUE,
    'level' => 'all'
];

$routes[] = [
	'method' => 'post',
	'route' => '/map/savepin',
	'handler' => ['Controllers\MapController', 'savepinAction'],
	'authentication' => TRUE,
    'level' => 'agent'
];

$routes[] = [
    'method' => 'post',
    'route' => '/map/deletepins/{id}',
    'handler' => ['Controllers\MapController', 'deletepinAction'],
    'authentication' => TRUE,
    'level' => 'all'
];
$routes[] = [
    'method' => 'post',
    'route' => '/{table}/like/{agent}/{map}/{like}',
    'handler' => ['Controllers\MapController', 'likeAction'],
    'authentication' => TRUE,
    'level' => 'agent'
];
$routes[] = [
    'method' => 'post',
    'route' => '/map/updatecover/{id}/{type}',
    'handler' => ['Controllers\MapController', 'updatecoverAction'],
    'authentication' => TRUE,
    'level' => 'agent'
];
$routes[] = [
    'method' => 'post',
    'route' => '/pin/addview/{id}',
    'handler' => ['Controllers\MapController', 'pinviewAction'],
    'authentication' => TRUE,
    'level' => 'agent'
];
$routes[] = [
    'method' => 'post',
    'route' => '/pin/setkingpin/{id}/{mapid}',
    'handler' => ['Controllers\MapController', 'setkingpinAction'],
    'authentication' => TRUE,
    'level' => 'all'
];
$routes[] = [
    'method' => 'get',
    'route' => '/getpin/images/{id}',
    'handler' => ['Controllers\MapController', 'getpinimagesAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/pin/updatevideo/{id}',
    'handler' => ["Controllers\MapController", 'updatevideoAction'],
    'authentication' => TRUE,
    'level' => 'all'
];
$routes[] = [
    'method' => 'post',
    'route' => '/pin/editdesc/{id}/{desc}',
    'handler' => ['Controllers\MapController', 'updatePinDescAction'],
    'authentication' => TRUE,
    'level' => 'agent'
];
$routes[] = [
    'method' => 'post',
    'route' => '/pin/deleteimg/{id}',
    'handler' => ['Controllers\MapController', 'deletepinimgAction'],
    'authentication' => TRUE,
    'level' => 'all'
];
$routes[] = [
    'method' => 'get',
    'route' => '/pin/hideagent/{id}/{hideagent}',
    'handler' => ['Controllers\MapController', 'hideagentAction'],
    'authentication' => TRUE,
    'level' => 'agent'
];
$routes[] = [
    'method' => 'post',
    'route' => '/pin/pendingpinAction/{id}/{actiontype}',
    'handler' => ['Controllers\MapController', 'pendingpinAction'],
    'authentication' => TRUE,
    'level' => 'all'
];
$routes[] = [
    'method' => 'get',
    'route' => '/map/getnewslist/{id}',
    'handler' => ['Controllers\MapController', 'getmapnewslistAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/savecomment/{slug}/{agent}/{type}/{pinid}',
    'handler' => ['Controllers\CommentController', 'savecommentAction'],
    'authentication' => TRUE,
    'level' => 'all'
];
$routes[] = [
    'method' => 'get',
    'route' => '/map/getnews/{id}/{limit}',
    'handler' => ['Controllers\MapController', 'getnewsAction'],
    'authentication' => FALSE
];
// ADMIN ROUTES - backend

$routes[] = [
    'method' => 'post',
    'route' => '/superagent/login',
    'handler' => ['Controllers\UserController', 'loginAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/superagent/missions/list/{num}/{page}/{keyword}/{status}',
    'handler' => ['Controllers\MapController', 'missionslistAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/superagent/getPinImages/{id}',
    'handler' => ['Controllers\MapController', 'bgetpinimagesAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/superagent/missionmap/{slug}',
    'handler' => ['Controllers\MapController', 'getmissionmapAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/superagent/missions/addcategory',
    'handler' => ['Controllers\MapController', 'addMapCatAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/superagent/getcategories',
    'handler' => ['Controllers\MapController', 'getmapcategoriesAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/superagent/titleexist/{id}/{title}',
    'handler' => ['Controllers\MapController', 'titlevalidationAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/superagent/loadmapgallery/{id}',
    'handler' => ['Controllers\MapController', 'loadmapgalleryAction'],
    'authentication' => TRUE,
    'level' => 'all'
];
$routes[] = [
    'method' => 'post',
    'route' => '/map/saveimage/{id}/{type}',
    'handler' => ['Controllers\MapController', 'saveimageAction'],
    'authentication' => TRUE,
    'level' => 'all'
];
$routes[] = [
    'method' => 'post',
    'route' => '/map/deletemapmedia/{id}/{type}',
    'handler' => ['Controllers\MapController', 'deletemapmediaAction'],
    'authentication' => TRUE,
    'level' => 'all'
];
$routes[] = [
    'method' => 'post',
    'route' => '/superagent/map/update/{id}',
    'handler' => ['Controllers\MapController', 'updatemapAction'],
    'authentication' => TRUE,
    'level' => 'all'
];
// MAP CATEGORY
$routes[] = [
    'method' => 'post',
    'route' => '/superagent/missions/category/add',
    'handler' => ['Controllers\MapController', 'addCategoryAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/superagent/missions/category/update',
    'handler' => ['Controllers\MapController', 'updateCategoryAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/superagent/missions/category/list/{num}/{page}/{keyword}',
    'handler' => ['Controllers\MapController', 'listCategoryAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'delete',
    'route' => '/superagent/missions/category/delete/{catid}',
    'handler' => ['Controllers\MapController', 'deleteCategoryAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/missions/category/get',
    'handler' => ['Controllers\MapController', 'getCategoriesAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/missions/news/get',
    'handler' => ['Controllers\MapController', 'getMainNewsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/map/deletenews/{id}',
    'handler' => ['Controllers\MapController', 'deletenewsAction'],
    'authentication' => TRUE,
    'level' => 'all'
];
$routes[] = [
    'method' => 'post',
    'route' => '/map/updatenews/{id}',
    'handler' => ['Controllers\MapController', 'updatenewsAction'],
    'authentication' => FALSE
];

//END MAP CATEGORY



// MAP TAGS ROUTES

$routes[] = [
    'method' => 'get',
    'route' => '/missions/tags/get',
    'handler' => ['Controllers\MapController', 'getTagsAction'],
    'authentication' => FALSE
];

// END TAGS ROUTES

$routes[] = [
    'method' => 'get',
    'route' => '/superagent/missions/fg/{val}/{id}/{type}',
    'handler' => ['Controllers\MapController', 'missionfeaturedAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/map/delete/{id}',
    'handler' => ['Controllers\MapController', 'deleteAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/index/missions',
    'handler' => ['Controllers\MapController', 'missionindexAction'],
    'authentication' => FALSE,
];
$routes[] = [
    'method' => 'get',
    'route' => '/index/newslist/{limit}',
    'handler' => ['Controllers\NewsController', 'getnewsAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/main/getauthor/{id}',
    'handler' => ['Controllers\NewsController', 'getauthorAction'],
    'authentication' => FALSE
];
// END ADMIN ROUTES

// $routes[] =[
//     'method'=>'get',
//     'route' => '/getpincomments/{id}',
//     'handler' => ['Controllers\CommentController', 'getpinrepliesAction'],
//     'authentication' => FALSE
// ];
$routes[] = [
    'method' => 'get',
    'route' => '/getpin/{id}',
    'handler' => ['Controllers\MapController', 'getpinAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/savepinreply/{markerid}/{agentid}/{mapid}',
    'handler' => ['Controllers\CommentController', 'savepinreplyAction'],
    'authentication' => TRUE,
    'level' => 'all'
];
$routes[] = [
    'method' => 'post',
    'route' => '/savecommentreply/{commentid}/{agentid}/{mapid}/{type}/{marker_id}/{offset}/{count}/{commentload}',
    'handler' => ['Controllers\CommentController', 'savecommentreplyAction'],
    'authentication' => 'post',
    'level' => 'all'
];
$routes[] = [
    'method' => 'get',
    'route' => '/getmapcomments/{mapid}/{limit}',
    'handler' => ['Controllers\CommentController', 'getmapcommentsAction'],
    'authentication' => FALSE
];
$routes[] = [
	'method' => 'get',
	'route' => '/getcommentreply/{id}/{offset}/{count}',
	'handler' => ['Controllers\CommentController', 'loadreplyAction'],
	'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/deletecomment/{id}/{type}',
    'handler' => ['Controllers\CommentController', 'deletecommentAction'],
    'authentication' => TRUE,
    'level' => 'all'
];
$routes[] = [
    'method' => 'post',
    'route' => '/editcomment/{id}/{type}',
    'handler' => ['Controllers\CommentController', 'editcommentAction'],
    'authentication' => TRUE,
    'level' => 'all'
];
$routes[] = [
    'method' => 'post',
    'route' => '/deleteresponse/{id}',
    'handler' => ['Controllers\CommentController', 'deleteresponseAction'],
    'authentication' => TRUE,
    'level' => 'all'
];
$routes[] = [
    'method' => 'post',
    'route' => '/editresponse/{id}',
    'handler' => ['Controllers\CommentController', 'editresponseAction'],
    'authentication' => TRUE,
    'level' => 'all'
];
//contact us
$routes[] = [
    'method' => 'post',
    'route' => '/frontend/savefeedback',
    'handler' => ['Controllers\ContactsController', 'savefeedbackAction'],
    'authentication' => FALSE
];

/*BACKEND*/
    /*NEWS*/
$routes[] = [
    'method' => 'get',
    'route' => '/news/validateSlug/{slug}/{id}',
    'handler' => ['Controllers\NewsController', 'validateslugAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/news/listimages',
    'handler' => ['Controllers\NewsController', 'listimageAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/news/listvideo',
    'handler' => ['Controllers\NewsController', 'listvideoAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/news/saveimage',
    'handler' => ['Controllers\NewsController', 'saveimageAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/news/addvid',
    'handler' => ['Controllers\NewsController', 'savevideoAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/news/deletenewsimg',
    'handler' => ['Controllers\NewsController', 'deletenewsimgAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/news/deletevideo',
    'handler' => ['Controllers\NewsController', 'deletevideoAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/news/listauthor',
    'handler' => ['Controllers\NewsController', 'listAuthorAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/news/listcategory',
    'handler' => ['Controllers\NewsController', 'listcategoryAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/news/listtags',
    'handler' => ['Controllers\NewsController', 'listtagsAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/news/create',
    'handler' => ['Controllers\NewsController', 'createNewsAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/news/managenews/{num}/{off}/{keyword}/{sort}',
    'handler' => ['Controllers\NewsController', 'manageNewsAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/news/newsdelete/{newsid}',
    'handler' => ['Controllers\NewsController', 'newsdeleteAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/news/updatenewsstatus/{status}/{newsid}',
    'handler' => ['Controllers\NewsController', 'newsUpdatestatusAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/news/newsedit/{newsid}',
    'handler' => ['Controllers\NewsController', 'newseditoAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/news/updatenews',
    'handler' => ['Controllers\NewsController', 'updateNewsAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
 'method' => 'post',
 'route' => '/news/updatenewsauthors',
 'handler' => ['Controllers\NewsController', 'updatenewsauthorsAction'],
 'authentication' => TRUE,
 'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/news/updatenewsfeatured/{id}/{featured}',
    'handler' => ['Controllers\NewsController', 'newsUpdateFeaturedAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/mapnews/save/{id}',
    'handler' => ['Controllers\NewsController', 'savemapnewsAction'],
    'authentication' => TRUE,
    'level' => 'agent'
];
        /*author*/
$routes[] = [
    'method' => 'post',
    'route' => '/news/saveauthorimage',
    'handler' => ['Controllers\NewsController', 'saveauthorimageAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/news/authorlistimages',
    'handler' => ['Controllers\NewsController', 'authorlistimagesAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/news/deleteauthorimg',
    'handler' => ['Controllers\NewsController', 'deleteauthorimgAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/news/createauthor',
    'handler' => ['Controllers\NewsController', 'createAuthorAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/news/manageauthor/{num}/{off}/{keyword}',
    'handler' => ['Controllers\NewsController', 'manageAuthorAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/news/loadauthornews/{id}',
    'handler' => ['Controllers\NewsController', 'loadauthornewsAction'],
    'authentication' => TRUE,
    'level' => 'all'
];
$routes[] = [
    'method' => 'post',
    'route' => '/news/authordelete/{authorid}',
    'handler' => ['Controllers\NewsController', 'authordeleteAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/news/editauthor/{authorid}',
    'handler' => ['Controllers\NewsController', 'authoreditoAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/news/editauthor',
    'handler' => ['Controllers\NewsController', 'authorupdateAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
        /*Category*/
$routes[] = [
    'method' => 'get',
    'route' => '/news/managecategory/{num}/{off}/{keyword}',
    'handler' => ['Controllers\NewsController', 'managecategoryAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/news/savecategory',
    'handler' => ['Controllers\NewsController', 'createcategoryAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/news/loadconflict/{id}',
    'handler' => ['Controllers\NewsController', 'loadconflictsAction'],
    'authentication' => TRUE,
    'level' => 'all'
];
$routes[] = [
    'method' => 'post',
    'route' => '/news/updateconflict',
    'handler' => ['Controllers\NewsController', 'updateconflictAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/news/categorydelete/{id}',
    'handler' => ['Controllers\NewsController', 'categorydeleteAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/news/updatecategorynames/{catname}/{id}',
    'handler' => ['Controllers\NewsController', 'updatecategoryAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
        /*Tags*/
$routes[] = [
    'method' => 'get',
    'route' => '/news/managetags/{num}/{off}/{keyword}',
    'handler' => ['Controllers\NewsController', 'managetagsAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/news/savetags',
    'handler' => ['Controllers\NewsController', 'createtagsAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/tags/loadconflict/{id}',
    'handler' => ['Controllers\NewsController', 'tagconflictAction'],
    'authentication' => TRUE,
    'level' => 'all'
];
$routes[] = [
    'method' => 'post',
    'route' => '/tags/updateconflict',
    'handler' => ['Controllers\NewsController', 'updateTagConflictAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/news/tagsdelete/{id}',
    'handler' => ['Controllers\NewsController', 'tagsdeleteAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/news/updatetags/{tagname}/{id}',
    'handler' => ['Controllers\NewsController', 'updatetagsAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
	// CONTACT US
$routes[] = [
	'method' => 'get',
	'route' => '/contacts/list/{num}/{off}/{keyword}/{date}',
	'handler' => ['Controllers\ContactsController', 'getlistAction'],
	'authentication' => TRUE,
	'level' => 'admin'
];
$routes[] = [
	'method' => 'post',
	'route' => "/contacts/delete/{id}",
	'handler' => ["Controllers\ContactsController", 'deleteAction'],
	'authentication' => TRUE,
	'level' => 'admin'
];
$routes[] = [
	'method' => 'get',
	'route' => '/contacts/get/{id}',
	'handler' => ['Controllers\ContactsController', 'getAction'],
	'authentication' => TRUE,
	'level' => 'admin'
];
$routes[] = [
	'method' => 'post',
	'route' => '/contacts/reply/{id}',
	'handler' => ['Controllers\ContactsController', 'replyAction'],
	'authentication' => TRUE,
	'level' => 'admin'
];


$routes[] = [
    'method' => 'get',
    'route' => '/get/totals',
    'handler' => ['Controllers\MapController', 'getTotalAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/get/all/pins',
    'handler' => ['Controllers\MapController', 'getAllPins'],
    'authentication' => FALSE
];

return $routes;
